package net.readybytes.taskscheduler.view;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import net.readybytes.taskscheduler.R;

public class SplashActivity extends AppCompatActivity {
    public static final int SPLASH_TIME_OUT = 500;
    TextView txtsplashtext;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        txtsplashtext = (TextView)findViewById(R.id.splashtext);
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {

                            Intent i = new Intent(SplashActivity.this, BaseActivity.class);
                            startActivity(i);

                // close this activity
                finish();
            }
        }, SPLASH_TIME_OUT);
        txtsplashtext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(SplashActivity.this,BaseActivity.class);
                startActivity(intent);
            }
        });


    }
}
