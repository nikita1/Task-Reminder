package net.readybytes.taskscheduler.adapter;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import net.readybytes.taskscheduler.Controller.AlarmManagerController;
import net.readybytes.taskscheduler.Controller.DatabaseHandling;
import net.readybytes.taskscheduler.Controller.LaunchTimePicker;
import net.readybytes.taskscheduler.Controller.ReminderCreationController;
import net.readybytes.taskscheduler.R;
import net.readybytes.taskscheduler.check_update_data;
import net.readybytes.taskscheduler.globalusage.Getters;
import net.readybytes.taskscheduler.model.ReminderDetails;

import java.sql.Time;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import static android.content.Context.ALARM_SERVICE;

/**
 * Created by rbsl on 22/8/17.
 */

public class NotificationTasklistAdapter extends ArrayAdapter<ReminderDetails> {
    Context context;
    int  mHour, mMinute;
    static final String TAG = "inAdapter";
    String countryCode;
    int layoutResourceId;
    DatabaseHandling databaseHandling;
    AlarmManager alarmManager;
    check_update_data update;
    long interval =0;
    Calendar calendar=Calendar.getInstance();
    ArrayList<ReminderDetails> data = new ArrayList<ReminderDetails>();
    public NotificationTasklistAdapter(Context context, int layoutResourceId, ArrayList<ReminderDetails> data,check_update_data updatenew) {
        super(context, layoutResourceId, data);
        this.layoutResourceId = layoutResourceId;
        this.context = context;
        this.data = data;
        this.update = updatenew;
    }

    public View getView(int position, final View convertView, ViewGroup parent) {
        View row = convertView;
        UserHolder holder = null;
        if (row == null) {
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);
            holder = new UserHolder();
            holder.txttasktime = (TextView) row.findViewById(R.id.tasktime);
            holder.txttaskname = (TextView) row.findViewById(R.id.taskname);
            holder.btnswitch = (Switch)row.findViewById(R.id.on_off_switch);
            holder.taskdate  = (TextView)row.findViewById(R.id.taskdate);
            row.setTag(holder);
        } else {
            holder = (UserHolder) row.getTag();
        }
        final ReminderDetails reminderDetails = data.get(position);
        holder.txttaskname.setText(reminderDetails.getReminderName());
        holder.txttasktime.setText(Getters.getStringwhenInput24hrs(context,reminderDetails.getReminderTime().toString()));
        holder.btnswitch.setChecked(reminderDetails.isReminderActive());
        holder.taskdate.setText(Getters.getdateStringddMMyyyy(context,reminderDetails.getReminderDate()));


        final UserHolder finalHolder = holder;
        holder.txttasktime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                final Calendar c = Calendar.getInstance();
                mHour = c.get(Calendar.HOUR);
                mMinute = c.get(Calendar.MINUTE);


                // Launch Time Picker Dialog


            }
        });
        holder.btnswitch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                alarmManager = (AlarmManager)context.getSystemService(ALARM_SERVICE);
                databaseHandling= new DatabaseHandling(context);
               if(!finalHolder.btnswitch.isChecked()) {
                   databaseHandling.update(finalHolder.btnswitch.isChecked(), reminderDetails,"enable|disable");
                   if(new AlarmManagerController().cancelSpecifiedAlarm(reminderDetails,context)){
                       Toast.makeText(context, "ALARM CANCELLED", Toast.LENGTH_SHORT).show();
                   //    update.onItemchange (true);
                   }

               }else{

                   if(Getters.getDateinyyyyMMddHHmmformatAsDateObject(context,reminderDetails.getReminderTime(),reminderDetails.getReminderDate()).compareTo(new Date())<0){
                       Toast.makeText(context, "First Edit the date again or Chose to set it again from the current Date ", Toast.LENGTH_LONG).show();
                       int hour = calendar.get(Calendar.HOUR_OF_DAY);
                       int minute = calendar.get(Calendar.MINUTE);
                       TimePickerDialog mTimePicker;
                       mTimePicker = new TimePickerDialog(context, new TimePickerDialog.OnTimeSetListener() {
                           @Override
                           public void onTimeSet(TimePicker timePicker, int hourOfDay, int minute) {

                               interval= new ReminderCreationController().calculateinterval(reminderDetails.getRemindertimePer(),calendar,context);
                               finalHolder.btnswitch.setChecked(true);
                               reminderDetails.setReminderDate(Getters.getdateasDateObject(context,finalHolder.taskdate.getText().toString()));
                               reminderDetails.setReminderTime(Getters.getTimeObjectwhenInputAMPM(context,Getters.getStringwhenInputhoursandminutes(hourOfDay,minute,"")));
                               databaseHandling.update(finalHolder.btnswitch.isChecked(), reminderDetails,"edit");
                               databaseHandling.update(finalHolder.btnswitch.isChecked(),reminderDetails,"enable|disable");
                               if (new AlarmManagerController().startalarmManager(context, reminderDetails, reminderDetails.getRequestCode(), alarmManager, reminderDetails.getReminderTime(), reminderDetails.getReminderDate(), interval,context.getResources().getString(R.string.action_update))) {
                                   Toast.makeText(context, "ALARM SET AGAIN" + reminderDetails.getReminderId(), Toast.LENGTH_SHORT).show();
                                   finalHolder.txttasktime.setText(Getters.getStringwhenInputhoursandminutes(hourOfDay,minute,""));
                               }
                           }
                       }, hour, minute, false);
                       mTimePicker.show();
                       new DatePickerDialog(context, new DatePickerDialog.OnDateSetListener() {
                           @Override
                           public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                               finalHolder.taskdate.setText(day+"/"+(++month)+"/"+year);

                           }
                       },calendar.get(Calendar.YEAR),calendar.get(Calendar.MONTH),calendar.get(Calendar.DAY_OF_MONTH)).show();
                       //have to reenter new time and date
                    //   update.onItemchange (true);
                   }else {
                       databaseHandling.update(finalHolder.btnswitch.isChecked(), reminderDetails,"enable|disable");
                       if (new AlarmManagerController().startalarmManager(context, reminderDetails, reminderDetails.getRequestCode(), alarmManager, reminderDetails.getReminderTime(), reminderDetails.getReminderDate(), interval,context.getResources().getString(R.string.action_update))) {
                           Toast.makeText(context, "ALARM SET AGAIN" + reminderDetails.getReminderId(), Toast.LENGTH_SHORT).show();
                            finalHolder.btnswitch.setChecked(true);
                          return;
                       }
                      // update.onItemchange (true);
                   }

               }
                finalHolder.btnswitch.setChecked(false);
            }
        });
        return row;

    }


    static class UserHolder {
        TextView txttaskname;
        TextView txttasktime;
        Switch btnswitch;
        TextView taskdate;

    }
}
