package net.readybytes.taskscheduler.broadcastrecievers;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;

import net.readybytes.taskscheduler.R;
import net.readybytes.taskscheduler.SessionManager;
import net.readybytes.taskscheduler.globalusage.Getters;
import net.readybytes.taskscheduler.model.ReminderDetails;

import java.util.Date;
import java.util.Random;
import java.util.logging.StreamHandler;

import static android.content.Context.NOTIFICATION_SERVICE;

public class AlarmReciever extends BroadcastReceiver {
    private NotificationManager alarmNotificationManager;
    SessionManager sessionManager;
    long requestCode;
    Bundle bundle;
    Date fordaynewDateFormat,forday ;
    ReminderDetails reminderDetails;
    private final String TAG ="AlarmsRecieverAgain";
    @Override
    public void onReceive(Context context, Intent intent) {
        // TODO: This method is called when the BroadcastReceiver is receiving
        // an Intent broadcast.
       try {
           requestCode= System.currentTimeMillis();
           fordaynewDateFormat= new Date();
           bundle = intent.getBundleExtra("RequestBundle");
           reminderDetails = (ReminderDetails) bundle.getSerializable("ReminderDetails");
           forday = Getters.getdateasDateObject(context, Getters.getdateStringddMMyyyy(context, new Date()));
           /**
            * Session related work over here
            */
           Log.d(TAG,reminderDetails.toString());
           Log.d(TAG,reminderDetails.getReminderName().toString());
           sessionManager = new SessionManager(context);
           sessionManager.setLastDate(reminderDetails.getReminderName().toString(), new Date());
           //this launches a notification ring when a alarmReciever is called
           Uri alarmUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
           Toast.makeText(context, "ALARM RECIEVER IS CALLED", Toast.LENGTH_SHORT).show();
           Ringtone ringtone = RingtoneManager.getRingtone(context, alarmUri);
           alarmUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
           ringtone.play();
           //two Intents are being fired here, first one is actionYesIntent
       }catch (NullPointerException e){
          Log.e("AlarmsRecieverAgain","Null pointer Exception in AlarmReciever");
           e.printStackTrace();
       }
        alarmNotificationManager = (NotificationManager) context.getSystemService(NOTIFICATION_SERVICE);
        Intent actionYesintent = new Intent(NOTIFICATION_SERVICE);
        actionYesintent.setAction("notification");
        Bundle yesBundle = new Bundle();
        yesBundle.putInt("userAnswer", 1);
        yesBundle.putInt("requestcode",(int)requestCode);
        yesBundle.putSerializable("date",forday);
        yesBundle.putSerializable("ReminderDetailObject",reminderDetails);
        actionYesintent.putExtras(yesBundle);
        Random r = new Random();
        int randomnumber = r.nextInt(100 - 1) + 1;
        PendingIntent pendingYesIntent = PendingIntent.getBroadcast(context, (int)requestCode+randomnumber,actionYesintent, PendingIntent.FLAG_UPDATE_CURRENT);

        /**
         * another intent is for NO button
        **/
        Intent actionNoIntent = new Intent(NOTIFICATION_SERVICE);
        actionNoIntent.setAction("notification");
        Bundle NoBundle = new Bundle();
        NoBundle.putInt("userAnswer", 0);
        NoBundle.putInt("requestcode",(int)requestCode);
        NoBundle.putSerializable("date",forday);
        NoBundle.putSerializable("ReminderDetailObject",reminderDetails);
        actionNoIntent.putExtras(NoBundle);

        PendingIntent pendingNoIntent = PendingIntent.getBroadcast(context, (int)requestCode, actionNoIntent, PendingIntent.FLAG_UPDATE_CURRENT);
       /* PendingIntent contentIntent = PendingIntent.getActivity(context,(int)(intent.getExtras().getLong("requestCode")),
                new Intent(context,BaseActivity.class), 0);*/
        Log.d("AlarmService","For date"+sessionManager.getLastDate(reminderDetails.getReminderName()));
        NotificationCompat.Builder alamNotificationBuilder = new NotificationCompat.Builder(
                context).setContentTitle(reminderDetails.getReminderName()).setSmallIcon(R.drawable.icons8googlealerts)
                .setStyle(new NotificationCompat.BigTextStyle().bigText(context.getResources().getString(R.string.notificationMessage)+" "+(reminderDetails.getServicemanName().equalsIgnoreCase("")?"Anonymous":reminderDetails.getServicemanName())))
                .addAction(R.drawable.iconscheckmark_48, "YES",pendingYesIntent)
                .addAction(R.drawable.icons8delete48, "No", pendingNoIntent)
                .setContentText("for "+Getters.getdateStringddMMyyyy(context,fordaynewDateFormat)/*reminderDetails.getReminderTime()+" & "+reminderDetails.getReminderDate()*/);
        alarmNotificationManager.notify((int)requestCode, alamNotificationBuilder.build());
        Log.d("AlarmService", "Notification sent."+"with"+requestCode);
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            alamNotificationBuilder.setSmallIcon(R.drawable.icons8googlealerts);
            alamNotificationBuilder.setColor(context.getResources().getColor(R.color.toolbarcolor));
        } else {
            alamNotificationBuilder.setSmallIcon(R.drawable.icons8googlealerts);
        }

    }
}
