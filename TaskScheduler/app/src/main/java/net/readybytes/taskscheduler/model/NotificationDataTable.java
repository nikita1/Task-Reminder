package net.readybytes.taskscheduler.model;

import java.sql.Time;
import java.util.Date;

/**
 * Created by siddharh on 30/8/17.
 */

public class NotificationDataTable {
    Time remindertimeNotificationData;
    Date reminderdateNotificationData;
    int NotificationUniqueId_perTask;
    double perTaskCost;
    String reminderTaskName;
    String reminderTaskId;
    boolean UserResponseOnNotification;
    double Unit;

    public String getWagesPerDayOrMonth() {
        return wagesPerDayOrMonth;
    }

    public void setWagesPerDayOrMonth(String wagesPerDayOrMonth) {
        this.wagesPerDayOrMonth = wagesPerDayOrMonth;
    }

    double CostPerUnit;
    String wagesPerDayOrMonth;
    public double getUnit() {
        return Unit;
    }

    public void setUnit(double unit) {
        Unit = unit;
    }

    public double getCostPerUnit() {
        return CostPerUnit;
    }

    public void setCostPerUnit(double costPerUnit) {
        CostPerUnit = costPerUnit;
    }

    public String getReminderTaskId() {
        return reminderTaskId;
    }

    public void setReminderTaskId(String reminderTaskId) {
        this.reminderTaskId = reminderTaskId;
    }


    public NotificationDataTable() {
    }

    public boolean isUserResponseOnNotification() {
        return UserResponseOnNotification;
    }

    public void setUserResponseOnNotification(boolean userResponseOnNotification) {
        UserResponseOnNotification = userResponseOnNotification;
    }



    public NotificationDataTable(String ReminderTaskId,String ReminderTaskName,Time remindertimeNotificationData, Date reminderdateNotificationData, double perTaskCost,boolean UserResponseOnNotification,double Unit,double costPerUnit,String wagesPerDayOrMonth) {
        this.reminderTaskId               = ReminderTaskId;
        this.reminderTaskName             = ReminderTaskName;
        this.remindertimeNotificationData = remindertimeNotificationData;
        this.reminderdateNotificationData = reminderdateNotificationData;
        this.perTaskCost                  = perTaskCost;
        this.UserResponseOnNotification   = UserResponseOnNotification;
        this.CostPerUnit                  = costPerUnit;
        this.Unit                         = Unit;
        this.wagesPerDayOrMonth           = wagesPerDayOrMonth;
    }

    public Time getRemindertimeNotificationData() {
        return remindertimeNotificationData;
    }

    public String getReminderTaskName() {
        return reminderTaskName;
    }

    public void setReminderTaskName(String reminderTaskName) {
        this.reminderTaskName = reminderTaskName;
    }

    public void setRemindertimeNotificationData(Time remindertimeNotificationData) {
        this.remindertimeNotificationData = remindertimeNotificationData;
    }

    public Date getReminderdateNotificationData() {
        return reminderdateNotificationData;
    }

    public void setReminderdateNotificationData(Date reminderdateNotificationData) {
        this.reminderdateNotificationData = reminderdateNotificationData;
    }

    public int getNotificationUniqueId_perTask() {
        return NotificationUniqueId_perTask;
    }

    public void setNotificationUniqueId_perTask(int notificationUniqueId_perTask) {
        NotificationUniqueId_perTask = notificationUniqueId_perTask;
    }

    public double getPerTaskCost() {
        return perTaskCost;
    }

    public void setPerTaskCost(double perTaskCost) {
        this.perTaskCost = perTaskCost;
    }
}
