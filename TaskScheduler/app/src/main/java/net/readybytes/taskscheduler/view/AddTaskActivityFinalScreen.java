package net.readybytes.taskscheduler.view;

import android.app.AlarmManager;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import net.readybytes.taskscheduler.Controller.ReminderCreationController;
import net.readybytes.taskscheduler.R;
import net.readybytes.taskscheduler.model.ReminderDetails;

import java.io.ByteArrayOutputStream;
import java.util.Calendar;
import java.util.Date;
import java.sql.Time;

public class AddTaskActivityFinalScreen extends AppCompatActivity implements View.OnClickListener {
String taskName;
String RepeatationType;
Time time;
TextInputLayout textInputLayoutAmountHint;
Spinner RepeatSpinnerOption,WagesPerDayOrMonth;
EditText VendorNameEditText;
EditText VendorNumberEditText,AmountEdittext, QuantityEdittext;
Date date;
    //by default Quantity will be 1,if no Quantity has been mentioned
double Quantity=1;
double costPerUnit;
LinearLayout UnitPerDayVisibilityTextViewAndEdittext,CheckBoxWithTextViewLinearLayout;
double perDayCost;
Button doneButton;
CheckBox QuantityCheckbox;
boolean setTrackQuantity;
Calendar calendar= Calendar.getInstance();
AlarmManager alarmManager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.addtasksecond);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

        //     doneButton = (Button)findViewById(R.id.donebutton);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.backblue);
        taskName         = getIntent().getStringExtra("TaskName");
        RepeatationType  =getIntent().getStringExtra("RepeatationType");
        time             =(Time)getIntent().getSerializableExtra("time");
        date             =(Date) getIntent().getSerializableExtra("date");
        alarmManager     = (AlarmManager) getSystemService(ALARM_SERVICE);
        VendorNameEditText = (EditText)findViewById(R.id.venderNameEdittext);
        VendorNumberEditText = (EditText)findViewById(R.id.venderMobileNumberEdittext);
        WagesPerDayOrMonth=(Spinner)findViewById(R.id.wagesPerDayOrMonthSpinner);
        UnitPerDayVisibilityTextViewAndEdittext =(LinearLayout)findViewById(R.id.inputLayout_UnitOnlyforDay);
        AmountEdittext    = (EditText)findViewById(R.id.venderPerDayPriceEdittext);
        textInputLayoutAmountHint   =(TextInputLayout)findViewById(R.id.Unit_text_input);
        QuantityEdittext = (EditText)findViewById(R.id.edtunit);
        CheckBoxWithTextViewLinearLayout=(LinearLayout) findViewById(R.id.checkboxwithTextviewLinearLayout);
        doneButton        = (Button)findViewById(R.id.donebutton);
        doneButton.setOnClickListener(this);
        QuantityCheckbox  =(CheckBox)findViewById(R.id.quantityCheckbox);
        if(RepeatationType.equalsIgnoreCase(getResources().getString(R.string.monthly))){
            CheckBoxWithTextViewLinearLayout.setVisibility(View.GONE);
        }
        if(!QuantityCheckbox.isChecked()){
          UnitPerDayVisibilityTextViewAndEdittext.setVisibility(View.GONE);
        }
        QuantityCheckbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if(isChecked){
                    UnitPerDayVisibilityTextViewAndEdittext.setVisibility(View.VISIBLE);
                }else{
                    QuantityEdittext.getText().clear();
                    UnitPerDayVisibilityTextViewAndEdittext.setVisibility(View.GONE);
                }
            }
        });
        if(RepeatationType.equalsIgnoreCase(getResources().getString(R.string.no_repeating))) {
        WagesPerDayOrMonth.setVisibility(View.GONE);
        UnitPerDayVisibilityTextViewAndEdittext.setVisibility(View.GONE);
        }else{
        WagesPerDayOrMonth.setVisibility(View.VISIBLE);
        }
        if(setTrackQuantity){
            UnitPerDayVisibilityTextViewAndEdittext.setVisibility(View.VISIBLE);
        }
    }
    public void addTaskintoReminderDetailsObject(){
        long requestCode=(long)System.currentTimeMillis();
        Bitmap largeIcon = BitmapFactory.decodeResource(getResources(), R.drawable.thumbsup);
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        largeIcon.compress(Bitmap.CompressFormat.JPEG, 100, stream);
        byte[] byteArray = stream.toByteArray();


        ReminderDetails reminderDetails = new ReminderDetails(taskName,
                true,
                VendorNameEditText.getText().toString(),
                VendorNumberEditText.getText().toString(),
                byteArray,
                RepeatationType,
                time,
                date,
                Double.parseDouble(String.format("%.2f",perDayCost)),
                1,
                requestCode,
                Double.parseDouble(String.format("%.2f",Quantity)),
                costPerUnit,
                WagesPerDayOrMonth.getSelectedItem().toString());
        new ReminderCreationController().addTasktoSqlliteAndLaunchAlarm(reminderDetails,AddTaskActivityFinalScreen.this,requestCode,alarmManager,time,date,getResources().getString(R.string.action_create),calendar);
    }
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.donebutton:

                Boolean validationchek = checkvalidation();
                if (validationchek) {

                    addTaskintoReminderDetailsObject();
                    Intent intent=new Intent();
                    intent.putExtra("result",true);
                    setResult(200,intent);
                    finish();

                } else {
                    return;
                }

                break;
        }
    }
    private Boolean checkvalidation() {
        String Vendorname                   =VendorNameEditText.getText().toString();
        String VendorNumber                 =VendorNumberEditText.getText().toString();
        String CostPerQuantity              =AmountEdittext.getText().toString();
        String SpinnerWagesPerDayOrMonth    =WagesPerDayOrMonth.getSelectedItem().toString();

        if(Vendorname.equalsIgnoreCase("")){
            VendorNameEditText.setError(this.getResources().getString(R.string.vendorNameEmpty));
            return false;
        }
        if(VendorNumber.equalsIgnoreCase("")){
            VendorNumberEditText.setError(this.getResources().getString(R.string.vendorNumberEmpty));
            return false;
        }
        if(CostPerQuantity.equalsIgnoreCase("")){
            AmountEdittext.setError(this.getResources().getString(R.string.CostPerQuantityEmpty));
            return false;
        }
        else {

         costPerUnit  =  (!AmountEdittext.getText().toString().equals(""))?Double.parseDouble(AmountEdittext.getText().toString()):0.0;


         if(UnitPerDayVisibilityTextViewAndEdittext.getVisibility()==View.VISIBLE){
            if(!QuantityEdittext.getText().toString().equalsIgnoreCase("")) {
                Quantity = Double.parseDouble(QuantityEdittext.getText().toString());
                perDayCost= Quantity*costPerUnit;
            }

            }
         perDayCost= Quantity*costPerUnit;
            if(WagesPerDayOrMonth.getSelectedItem().toString().equalsIgnoreCase("month")){
                //dividing the whole amount by number of days,and assuming it ideally as 30;
                perDayCost/=30;
            }
         return  true;
     }

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.addtaskmenu, menu);
        return true;
    }

    @Override
    public void onBackPressed() {

        super.onBackPressed();
        overridePendingTransition(R.anim.animation_enter, R.anim.animation_exit);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if(id == android.R.id.home){

            finish();
            overridePendingTransition(R.anim.animation_enter, R.anim.animation_exit);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    }
