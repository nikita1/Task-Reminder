package net.readybytes.taskscheduler.Controller;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.icu.text.SimpleDateFormat;
import android.util.Log;
import android.widget.Toast;

import net.readybytes.taskscheduler.globalusage.Getters;
import net.readybytes.taskscheduler.model.NotificationDataTable;
import net.readybytes.taskscheduler.model.ReminderDetails;

import java.sql.Time;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by siddharh on 23/8/17.
 */

public class DatabaseHandling extends SQLiteOpenHelper {

    // All Static variables
    // Database Version
    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "SCHEDULETASK";

    // products table name
    private static final String SCHEDULE_TASK = "scheduleTask";
    private static final String TAG = "Insert query";
    // Clients Table Columns names
    static int reminder_id=0;
    private static final String KEY_REMINDERID              = "reminder_id";
    private static final String KEY_REMINDERNAME            = "reminder_name";
    private static final String KEY_ISREMINDERACTIVE        = "isTask_available";
    private static final String KEY_SERVICEMAN_NAME         = "serviceman_name";
    private static final String KEY_SERVICEMAN_NUMBER       = "serviceman_number";
    private static final String KEY_SERVICEMAN_PHOTO        = "serviceman_photo";
    private static final String KEY_REMINDER_TIME           = "reminder_time";
    private static final String KEY_REMINDER_DATE           = "reminder_date";
    private static final String KEY_REMINDERTIME_PERCHOICE  = "remindertime_per";
    private static final String KEY_PERDAYCOST              = "per_day_cost";
    private static final String KEY_REPEATPERDAY            = "repeat_per_day";
    private static final String KEY_REQUESTCODE             = "request_code";
    private static final String KEY_REMINDER_NOTIFICATION_ID= "reminder_notification_id";
    private static final String KEY_NOTIFICATION_YES_or_NO  = "user_response";
    private static final String KEY_UNIT                    = "unit";
    private static final String KEY_COST_PER_UNIT           = "cost_per_unit";
    private static final String KEY_WAGES_PER_DAY_OR_MONTH  = "wages_per_day_or_month";

    SQLiteDatabase db;




    String CREATE_PRODUCT_TABLE = "CREATE TABLE " + SCHEDULE_TASK + "("
            +KEY_REMINDERID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            +KEY_REMINDERNAME + " TEXT,"
            +KEY_ISREMINDERACTIVE + " BOOLEAN,"
            +KEY_SERVICEMAN_NAME +" TEXT,"
            +KEY_SERVICEMAN_NUMBER+" TEXT,"
            +KEY_SERVICEMAN_PHOTO+" BLOB,"
            +KEY_REMINDER_TIME+" TEXT,"
            +KEY_REMINDERTIME_PERCHOICE+" TEXT,"
            +KEY_REMINDER_DATE+" TEXT,"
            +KEY_PERDAYCOST+" DOUBLE,"
            +KEY_REPEATPERDAY+" INTEGER,"
            +KEY_REQUESTCODE+" TEXT,"
            +KEY_UNIT+" DOUBLE DEFAULT 1,"
            +KEY_COST_PER_UNIT+" DOUBLE DEFAULT 0,"
            +KEY_WAGES_PER_DAY_OR_MONTH+" TEXT)";


    public DatabaseHandling(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        db = getWritableDatabase();

    }

    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL(CREATE_PRODUCT_TABLE);
    }

    // Upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + SCHEDULE_TASK);
        // Create tables again
        onCreate(db);
    }

    //IT IS FOR INSERTING THE USER IN THE DATABASE
    public void addUsers(ReminderDetails reminderDetails) throws SQLException{
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();


        values.put(KEY_REMINDERNAME, reminderDetails.getReminderName());
        values.put(KEY_ISREMINDERACTIVE, reminderDetails.isReminderActive());
        values.put(KEY_SERVICEMAN_NAME,reminderDetails.getServicemanName());
        values.put(KEY_SERVICEMAN_NUMBER,reminderDetails.getServicemanNumber());
        values.put(KEY_SERVICEMAN_PHOTO,reminderDetails.getServicemanPhoto());
        values.put(KEY_REMINDER_TIME,reminderDetails.getReminderTime().toString());
        values.put(KEY_REMINDERTIME_PERCHOICE,reminderDetails.getRemindertimePer().toString());
        values.put(KEY_REMINDER_DATE,reminderDetails.getReminderDate().toString());
        values.put(KEY_PERDAYCOST,String.valueOf(reminderDetails.getCostPerDay()));
        values.put(KEY_REPEATPERDAY,reminderDetails.getRepeatPerday());
        values.put(KEY_REQUESTCODE,reminderDetails.getRequestCode());
        values.put(KEY_UNIT,reminderDetails.getUnit());
        values.put(KEY_COST_PER_UNIT,reminderDetails.getCostPerUnit());
        values.put(KEY_WAGES_PER_DAY_OR_MONTH,reminderDetails.getWagesPerDayorMonth());
        // Inserting Row
        db.insert(SCHEDULE_TASK, null, values);
        Cursor cursor = db.rawQuery("select * from " + SCHEDULE_TASK, null);
        cursor.moveToLast();
        int reminder_id = cursor.getInt(0);
        reminderDetails.setReminderId(String.valueOf(reminder_id));
        db.close();
        // Closing database connection

    }

    public List<ReminderDetails> getListofallClients() {
        List<ReminderDetails> tasklistList = new ArrayList<ReminderDetails>();
        // Select All Query
        String selectQuery = "SELECT * FROM " + SCHEDULE_TASK;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                ReminderDetails reminderDetails = new ReminderDetails();
                reminderDetails.setReminderId(cursor.getString(0));
                reminderDetails.setReminderName(cursor.getString(1));
                reminderDetails.setReminderActive(cursor.getString(2).equals("1")?true:false);
                reminderDetails.setServicemanName(cursor.getString(3));
                reminderDetails.setServicemanNumber(cursor.getString(4));
                reminderDetails.setServicemanPhoto(cursor.getBlob(5));
                reminderDetails.setReminderTime(Time.valueOf(cursor.getString(6)));
                reminderDetails.setRemindertimePer(cursor.getString(7));
                reminderDetails.setReminderDate(new Date(cursor.getString(8)));
                reminderDetails.setCostPerDay(Double.parseDouble(cursor.getString(9)));
                reminderDetails.setRepeatPerday(cursor.getString(10)!=null?Integer.parseInt(cursor.getString(10)):0);
                reminderDetails.setRequestCode(Long.parseLong(cursor.getString(11)));
                reminderDetails.setUnit(cursor.getDouble(12));
                reminderDetails.setCostPerUnit(cursor.getDouble(13));
                reminderDetails.setWagesPerDayorMonth(cursor.getString(14));
                // Adding contact to list
                tasklistList.add(reminderDetails);
            } while (cursor.moveToNext());
        }
        // return contact list
        return tasklistList;
    }

    public int update(boolean ischecked,ReminderDetails reminderDetails,String Action) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        if(Action.equals("enable|disable")) {
            values.put(KEY_ISREMINDERACTIVE, ischecked);
        }
        else if(Action.equals("edit")) {
            values.put(KEY_REMINDERNAME, reminderDetails.getReminderName());
            values.put(KEY_SERVICEMAN_NAME,reminderDetails.getServicemanName());
            values.put(KEY_SERVICEMAN_NUMBER,reminderDetails.getServicemanNumber());
            values.put(KEY_REMINDER_TIME,reminderDetails.getReminderTime().toString());
            values.put(KEY_REMINDERTIME_PERCHOICE,reminderDetails.getRemindertimePer().toString());
            values.put(KEY_REMINDER_DATE,reminderDetails.getReminderDate().toString());
            values.put(KEY_PERDAYCOST,String.valueOf(reminderDetails.getCostPerDay()));
            values.put(KEY_UNIT,reminderDetails.getUnit());
            values.put(KEY_COST_PER_UNIT,reminderDetails.getCostPerUnit());
            values.put(KEY_WAGES_PER_DAY_OR_MONTH,reminderDetails.getWagesPerDayorMonth());
        }

        // updating row
        return db.update(SCHEDULE_TASK, values, KEY_REMINDERID + " = ?",
                new String[]{reminderDetails.getReminderId()});
    }

    public void deleteTask(ReminderDetails reminderDetails) {
        List<String> listOfTablesToDelete;
        SQLiteDatabase db = this.getWritableDatabase();
        listOfTablesToDelete =deleteNotificationTable(reminderDetails);
        for (String tableName :
                listOfTablesToDelete) {
            db.execSQL("DROP TABLE IF EXISTS " + tableName);
            Log.d("DatabaseHandling","Deleted table "+tableName);
        }

        db.delete(SCHEDULE_TASK, KEY_REMINDERID + " = ?",
                new String[]{reminderDetails.getReminderId()});
        db.close();

    }

    public void createNotificationTable(NotificationDataTable notificationDataTable,ReminderDetails reminderDetails) {
        final SQLiteDatabase db = getWritableDatabase();

        String CREATE_TABLE_NEW_USER = "CREATE TABLE IF NOT EXISTS " + "tasktable"+reminderDetails.getReminderId()+getStringFormatforMonth(notificationDataTable)+"("
                +KEY_REMINDER_NOTIFICATION_ID+" INTEGER PRIMARY KEY AUTOINCREMENT,"
                +KEY_REMINDERNAME+" TEXT,"
                +KEY_REMINDERID+" TEXT,"
                +KEY_REMINDER_TIME+" TEXT,"
                +KEY_REMINDER_DATE+" TEXT,"
                +KEY_NOTIFICATION_YES_or_NO+" BOOLEAN,"
                +KEY_PERDAYCOST+" DOUBLE,"
                +KEY_UNIT+" DOUBLE,"
                +KEY_COST_PER_UNIT+" DOUBLE,"
                +KEY_WAGES_PER_DAY_OR_MONTH+" TEXT)";
        db.execSQL(CREATE_TABLE_NEW_USER);
        db.close();
    }
   public long insertNotificationTable(Context context,NotificationDataTable notificationDataTable,ReminderDetails reminderDetails) {
       long a = 0;
       try {

          List<NotificationDataTable> notificationDataTableList = getListofNotificationData(context, reminderDetails, Getters.getMonthMMMyearFormat(context, reminderDetails.getReminderDate()));
          if (notificationDataTableList.size() == 0) {

              ContentValues values = new ContentValues();
              values.put(KEY_REMINDERNAME, reminderDetails.getReminderName());
              values.put(KEY_REMINDERID, reminderDetails.getReminderId());
              values.put(KEY_REMINDER_TIME, notificationDataTable.getRemindertimeNotificationData().toString());
              values.put(KEY_REMINDER_DATE, notificationDataTable.getReminderdateNotificationData().toString());
              values.put(KEY_NOTIFICATION_YES_or_NO, notificationDataTable.isUserResponseOnNotification());
              values.put(KEY_PERDAYCOST, notificationDataTable.getPerTaskCost());
              values.put(KEY_UNIT, notificationDataTable.getUnit());
              values.put(KEY_COST_PER_UNIT, notificationDataTable.getCostPerUnit());
              values.put(KEY_WAGES_PER_DAY_OR_MONTH,reminderDetails.getWagesPerDayorMonth());
              // Inserting Row
              a = db.insert("tasktable" + reminderDetails.getReminderId() + getStringFormatforMonth(notificationDataTable), null, values);
              Cursor cursor = db.rawQuery("select * from " + "tasktable" + reminderDetails.getReminderId() + getStringFormatforMonth(notificationDataTable), null);
              cursor.moveToLast();
              reminder_id = cursor.getInt(0);
              notificationDataTable.setNotificationUniqueId_perTask(reminder_id);

          }
          if (notificationDataTableList.size() > 0) {
              for (NotificationDataTable notificationDataTableForIteration :
                      notificationDataTableList) {
                  Log.d("DatabaseHandling", notificationDataTable.getReminderdateNotificationData().toString());
                  //if the date are found to be similar ,that means that has entered before
                  if (notificationDataTableForIteration.getReminderdateNotificationData().compareTo(notificationDataTable.getReminderdateNotificationData()) == 0) {
                      if (notificationDataTable.getNotificationUniqueId_perTask() == 0) {
                          return updateNotificationTableifDateisUniquekey(notificationDataTable, reminderDetails, "update");
                      } else {
                          return updateNotificationTableForATask(notificationDataTable, reminderDetails, "update");
                      }
                  }

              }
              //and insert the normal one >>
              SQLiteDatabase db = this.getWritableDatabase();
              ContentValues values = new ContentValues();
              values.put(KEY_REMINDERNAME, reminderDetails.getReminderName());
              values.put(KEY_REMINDERID, reminderDetails.getReminderId());
              values.put(KEY_REMINDER_TIME, notificationDataTable.getRemindertimeNotificationData().toString());
              values.put(KEY_REMINDER_DATE, notificationDataTable.getReminderdateNotificationData().toString());
              values.put(KEY_NOTIFICATION_YES_or_NO, notificationDataTable.isUserResponseOnNotification());
              values.put(KEY_PERDAYCOST, notificationDataTable.getPerTaskCost());
              values.put(KEY_UNIT, notificationDataTable.getUnit());
              values.put(KEY_COST_PER_UNIT, notificationDataTable.getCostPerUnit());
              values.put(KEY_WAGES_PER_DAY_OR_MONTH,reminderDetails.getWagesPerDayorMonth());
              // Inserting Row
              a = db.insert("tasktable" + reminderDetails.getReminderId() + getStringFormatforMonth(notificationDataTable), null, values);
              Cursor cursor = db.rawQuery("select * from " + "tasktable" + reminderDetails.getReminderId() + getStringFormatforMonth(notificationDataTable), null);
              cursor.moveToLast();
              reminder_id = cursor.getInt(0);
              notificationDataTable.setNotificationUniqueId_perTask(reminder_id);
              db.close();
              /* if (notificationDataTableForIteration.getReminderdateNotificationData().compareTo(notificationDataTable.getReminderdateNotificationData()) == 0) {
                   updateNotificationTableForATask(notificationDataTable, reminderDetails, "update");
               }else if(notificationDataTableForIteration.getReminderdateNotificationData().compareTo(notificationDataTable.getReminderdateNotificationData())>0){

               }*/
          }
          return a;
      }catch (SQLiteException e){
          Toast.makeText(context, "Error while inserting", Toast.LENGTH_SHORT).show();
          e.printStackTrace();
            return a;

      }
   }
   public List<String> deleteNotificationTable(ReminderDetails reminderDetails){
       List<String> listOfTable =  new ArrayList<>();
       String selectQuery = "SELECT * FROM " + " sqlite_sequence where name LIKE 'tasktable"+reminderDetails.getReminderId()+"%'";
       SQLiteDatabase db = this.getWritableDatabase();
       Cursor cursor    = db.rawQuery(selectQuery,null);
       if (cursor.moveToFirst()) {
         do {
            listOfTable.add(cursor.getString(0));
              } while (cursor.moveToNext());
       }

       //db.execSQL("DROP TABLE IF EXISTS " + "tasktable"+reminderDetails.getReminderId()+getStringFormatforMonth(notificationDataTable));
        return listOfTable;
   }


    public List<NotificationDataTable> getListofNotificationData(Context context,ReminderDetails reminderDetails,String monthYear) {
        List<NotificationDataTable> tasklistList = new ArrayList<NotificationDataTable>();

        try {
            // Select All Query
            String selectQuery = "SELECT * FROM " + "tasktable" + reminderDetails.getReminderId() + monthYear;
            SQLiteDatabase db = this.getWritableDatabase();
            Cursor cursor = db.rawQuery(selectQuery, null);
            // looping through all rows and adding to list
            if (cursor.moveToFirst()) {
                do {
                    NotificationDataTable notificationDataTableforLocalScope = new NotificationDataTable();
                    notificationDataTableforLocalScope.setNotificationUniqueId_perTask(cursor.getInt(0));
                    notificationDataTableforLocalScope.setReminderTaskName(cursor.getString(1));
                    notificationDataTableforLocalScope.setReminderTaskId(cursor.getString(2));
                    notificationDataTableforLocalScope.setRemindertimeNotificationData(Time.valueOf(cursor.getString(3)));
                    notificationDataTableforLocalScope.setReminderdateNotificationData(new Date(cursor.getString(4)));
                    notificationDataTableforLocalScope.setUserResponseOnNotification(cursor.getString(5).equals("1") ? true : false);
                    notificationDataTableforLocalScope.setPerTaskCost(cursor.getDouble(6));
                    notificationDataTableforLocalScope.setUnit(cursor.getDouble(7));
                    notificationDataTableforLocalScope.setCostPerUnit(cursor.getDouble(8));
                    notificationDataTableforLocalScope.setWagesPerDayOrMonth(cursor.getString(9));
                    // Adding contact to list
                    tasklistList.add(notificationDataTableforLocalScope);
                } while (cursor.moveToNext());
            }
            // return contact list
            return tasklistList;
        }catch (SQLiteException e){
            Toast.makeText(context, "No calender record uptil now", Toast.LENGTH_SHORT).show();
            e.printStackTrace();
            return tasklistList;
        }
    }
     public int updateNotificationTableForATask(NotificationDataTable notificationDataTable,ReminderDetails reminderDetails,String action){
         SQLiteDatabase db = this.getWritableDatabase();
         ContentValues values = new ContentValues();

         if(action.equals("update")) {
             values.put(KEY_REMINDERNAME, reminderDetails.getReminderName());
             values.put(KEY_REMINDERID,reminderDetails.getReminderId());
             values.put(KEY_REMINDER_TIME,notificationDataTable.getRemindertimeNotificationData().toString());
             values.put(KEY_REMINDER_DATE,notificationDataTable.getReminderdateNotificationData().toString());
             values.put(KEY_NOTIFICATION_YES_or_NO,notificationDataTable.isUserResponseOnNotification());
             values.put(KEY_PERDAYCOST,notificationDataTable.getPerTaskCost());
             values.put(KEY_UNIT,notificationDataTable.getUnit());
             values.put(KEY_COST_PER_UNIT,notificationDataTable.getCostPerUnit());
             values.put(KEY_WAGES_PER_DAY_OR_MONTH,notificationDataTable.getWagesPerDayOrMonth());
         }

         // updating row
         return db.update("tasktable"+reminderDetails.getReminderId()+getStringFormatforMonth(notificationDataTable), values, KEY_REMINDER_NOTIFICATION_ID + " = ?",
                 new String[]{String.valueOf(notificationDataTable.getNotificationUniqueId_perTask())});
     }

     public int updateNotificationTableifDateisUniquekey(NotificationDataTable notificationDataTable,ReminderDetails reminderDetails,String action){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        if(action.equals("update")) {
            values.put(KEY_REMINDERNAME, reminderDetails.getReminderName());
            values.put(KEY_REMINDERID,reminderDetails.getReminderId());
            values.put(KEY_REMINDER_TIME,notificationDataTable.getRemindertimeNotificationData().toString());
            values.put(KEY_NOTIFICATION_YES_or_NO,notificationDataTable.isUserResponseOnNotification());
            values.put(KEY_PERDAYCOST,notificationDataTable.getPerTaskCost());
            values.put(KEY_UNIT,notificationDataTable.getUnit());
            values.put(KEY_COST_PER_UNIT,notificationDataTable.getCostPerUnit());
            values.put(KEY_WAGES_PER_DAY_OR_MONTH,notificationDataTable.getWagesPerDayOrMonth());
        }

        // updating row
        return db.update("tasktable"+reminderDetails.getReminderId()+getStringFormatforMonth(notificationDataTable), values, KEY_REMINDER_DATE + " = ?",
                new String[]{notificationDataTable.getReminderdateNotificationData().toString()});
    }



    public LinkedList<String> getListofTasksName() {
        LinkedList<String> tasklistList = new LinkedList<String>();
        // Select All Query
        String selectQuery = "SELECT * FROM "+SCHEDULE_TASK;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
        String taskname= cursor.getString(1);
                // Adding contact to list
                tasklistList.add(taskname);
            } while (cursor.moveToNext());
        }
        // return contact list
        return tasklistList;
    }


   /* // TO GET A PARTICULAR USER'S DETAILS
    public Clients getContact(String emailid) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(Clientdetails, new String[]{KEY_ID,
                        KEY_NAME, KEY_PH_NO, KEY_ADDRESS}, KEY_ID + "=?",
                new String[]{String.valueOf(emailid)}, null, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();
        Clients client = new Clients(cursor.getString(0),
                cursor.getString(1), cursor.getString(2), cursor.getString(3));
        return client;
    }*/


  /*  // Updating single contact

*/


public String getStringFormatforMonth(NotificationDataTable notificationDataTable){
    java.text.SimpleDateFormat simpleDateFormat = new java.text.SimpleDateFormat("MMM");
    java.text.SimpleDateFormat simpleDateFormat1=new java.text.SimpleDateFormat("yyyy");
    String month = simpleDateFormat.format(notificationDataTable.getReminderdateNotificationData());
    String year  = simpleDateFormat1.format(notificationDataTable.getReminderdateNotificationData());
    return month+year;
}


}
