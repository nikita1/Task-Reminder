package net.readybytes.taskscheduler.Controller;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ParseException;
import android.os.Bundle;
import android.text.format.DateUtils;
import android.text.format.Time;
import android.text.method.DateTimeKeyListener;
import android.util.Log;
import android.widget.TimePicker;
import android.widget.Toast;

import net.readybytes.taskscheduler.R;
import net.readybytes.taskscheduler.broadcastrecievers.AlarmReciever;
import net.readybytes.taskscheduler.globalusage.Getters;
import net.readybytes.taskscheduler.model.ReminderDetails;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by siddharh on 23/8/17.
 */

public class AlarmManagerController {

    private PendingIntent pendingIntent;
    Calendar calendar;

    //call getCalenderInstance before startAlarmManager
    public boolean startalarmManager(Context context, ReminderDetails reminderDetails, long requestCode, AlarmManager alarmManager, java.sql.Time time, Date date, long interval,String action) {
        try {
            Date currentDate = new Date();
            int hours = new Date().getHours();
            int minutes=new Date().getMinutes();
            java.sql.Time timeforcomparison = java.sql.Time.valueOf(hours+":"+minutes+":00");

              Date selectedDate = Getters.getDateinyyyyMMddHHmmformatAsDateObject(context,time,date);


                if(selectedDate.compareTo(Getters.getDateinyyyyMMddHHmmformatAsDateObject(context,timeforcomparison,currentDate))<0) {
               /* Toast.makeText(context, "Trying to be OverSmart>>> Date should be after present day", Toast.LENGTH_LONG).show();
                throw new RuntimeException();*/
                    calendar = Calendar.getInstance();
                    calendar.setTime(Getters.getDateinyyyyMMddHHmmformatAsDateObject(context, time, new Date()));
                    selectedDate = calendar.getTime();
                }
            Intent myIntent = new Intent(context, AlarmReciever.class);
            Bundle bundle = new Bundle();
            bundle.putSerializable("ReminderDetails",reminderDetails);
            bundle.putLong("requestCode", requestCode);
            myIntent.putExtra("RequestBundle",bundle);
            launchAlarmwithSpecifiedIntents(myIntent,reminderDetails,context,requestCode,alarmManager,selectedDate,interval,action);
            return true;
        }catch (RuntimeException e){
            e.printStackTrace();
            return  false;
        }

        }
        public void launchAlarmwithSpecifiedIntents(Intent myIntent,ReminderDetails reminderDetails,Context context,long requestCode,AlarmManager alarmManager,Date currentDate,long interval,String action) {

            if (interval != 0) {
                if (action.equals(context.getResources().getString(R.string.action_create))) {
                    DatabaseHandling databaseHandling = new DatabaseHandling(context);
                    databaseHandling.addUsers(reminderDetails);
                    pendingIntent = PendingIntent.getBroadcast(context, (int) requestCode, myIntent, PendingIntent.FLAG_UPDATE_CURRENT);
                }

            if (action.equals(context.getResources().getString(R.string.action_update))) {
                pendingIntent = PendingIntent.getBroadcast(context, (int) requestCode, myIntent, PendingIntent.FLAG_CANCEL_CURRENT);
            }
            alarmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP, currentDate.getTime(),
                    interval, pendingIntent);
        }else if(interval==0&&(!action.equalsIgnoreCase(context.getResources().getString(R.string.action_update)))){
                Log.d("AlarmManagerController","action Not update && interval=0");
                DatabaseHandling databaseHandling = new DatabaseHandling(context);
                databaseHandling.addUsers(reminderDetails);
                pendingIntent = PendingIntent.getBroadcast(context, (int) requestCode, myIntent, PendingIntent.FLAG_CANCEL_CURRENT);
                alarmManager.set(AlarmManager.RTC_WAKEUP, currentDate.getTime(),
                         pendingIntent);
            }else if(interval==0&&(action.equalsIgnoreCase(context.getResources().getString(R.string.action_update)))){
                Log.d("AlarmManagerController","action update && interval=0");
                pendingIntent = PendingIntent.getBroadcast(context, (int) requestCode, myIntent, PendingIntent.FLAG_CANCEL_CURRENT);
                alarmManager.set(AlarmManager.RTC_WAKEUP, currentDate.getTime(),
                        pendingIntent);
            }
}



        public boolean cancelSpecifiedAlarm(ReminderDetails reminderDetails,Context context){
            try{
                AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
                Intent myIntent = new Intent(context.getApplicationContext(),
                        AlarmReciever.class);
                 PendingIntent pendingIntent = PendingIntent.getBroadcast(
                        context.getApplicationContext(), (int)reminderDetails.getRequestCode(), myIntent,PendingIntent.FLAG_CANCEL_CURRENT);

                alarmManager.cancel(pendingIntent);
                return true;
            }catch (Exception e){
                return false;
            }
        }
}
