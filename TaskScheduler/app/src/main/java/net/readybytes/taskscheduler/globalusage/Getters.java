package net.readybytes.taskscheduler.globalusage;

import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import java.sql.Time;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by siddharh on 30/8/17.
 */

public class Getters {

    public static String getdateStringddMMyyyy(Context context, Date date) {
        SimpleDateFormat date1 = new SimpleDateFormat("dd/MM/yyyy");
        String date3 = date1.format(date);
        return date3;
    }

    public static Date getdateasDateObject(Context context, String date) {
        try {
            return new SimpleDateFormat("dd/MM/yyyy").parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
            Toast.makeText(context, "OOPs!! Error with Date", Toast.LENGTH_SHORT).show();
            return null;

        }

    }

    public static Time getTimeObjectwhenInputAMPM(Context context, String time) {
        try {
            Time timenew;
            SimpleDateFormat date12Format = new SimpleDateFormat("hh:mm a");
            SimpleDateFormat date24Format = new SimpleDateFormat("HH:mm:00");
            timenew = Time.valueOf(date24Format.format(date12Format.parse(time)));
            return timenew;
        } catch (ParseException e) {
            e.printStackTrace();
            Toast.makeText(context, "OOPs!! Error with time", Toast.LENGTH_SHORT).show();
            return null;
        }
    }
    public static String getStringwhenInput24hrs(Context context, String time) {
            int hourOfDay,minute;
            String format="";
            Time timeAMPMFormat;
            String timenew=time;

            timeAMPMFormat =Time.valueOf(timenew);
             hourOfDay= timeAMPMFormat.getHours();
             minute = timeAMPMFormat.getMinutes();

            return  getStringwhenInputhoursandminutes(hourOfDay,minute,format);


    }
    public static String getStringwhenInputhoursandminutes(int hourOfDay,int minute,String format){
        String timenew;
        if (hourOfDay == 0) {

            hourOfDay += 12;

            format = "AM";
        } else if (hourOfDay == 12) {

            format = "PM";

        } else if (hourOfDay > 12)
        {

            hourOfDay -= 12;

            format = "PM";

        } else {

            format = "AM";
        }
        timenew=String.format("%02d:%02d%s", hourOfDay, minute," "+format);
    return timenew;
    }

    public static Date getDateinyyyyMMddHHmmformatAsDateObject(Context context,Time time,Date date){
        Date currentDate = new Date();
        SimpleDateFormat date1 = new SimpleDateFormat("yyyy/MM/dd");
        SimpleDateFormat date2 = new SimpleDateFormat("HH:mm");
        String date3 = date1.format(date);
        String date4 = date2.format(time);
        String myDate = date3 + " " + date4;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm");
        try {
            currentDate = sdf.parse(myDate);
            return currentDate;
        }catch (ParseException e){
            e.printStackTrace();
            Toast.makeText(context, "OOPS!! date format failed", Toast.LENGTH_SHORT).show();
            return new Date();
        }

    }
    public static String getMonthMMMyearFormat(Context context, Date date)
    {
        java.text.SimpleDateFormat simpleDateFormat = new java.text.SimpleDateFormat("MMM");
        java.text.SimpleDateFormat simpleDateFormat1=new java.text.SimpleDateFormat("yyyy");
        String month = simpleDateFormat.format(date);
        String year  = simpleDateFormat1.format(date);
        return month+year;
    }

}
