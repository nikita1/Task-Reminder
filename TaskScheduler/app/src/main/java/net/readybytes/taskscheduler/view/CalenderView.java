package net.readybytes.taskscheduler.view;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import net.readybytes.taskscheduler.Controller.AddTaskActivityController;
import net.readybytes.taskscheduler.R;
import net.readybytes.taskscheduler.adapter.CalenderViewAdapter;
import net.readybytes.taskscheduler.check_update_data;
import net.readybytes.taskscheduler.globalusage.Getters;
import net.readybytes.taskscheduler.model.ExpandableHeightGridView;
import net.readybytes.taskscheduler.model.NotificationDataTable;
import net.readybytes.taskscheduler.model.ReminderDetails;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class CalenderView extends AppCompatActivity implements checkCalenderdata{
    private static final String TAG = CalenderView.class.getSimpleName();
    private ImageView previousButton, nextButton;
    private TextView currentDate,TotalWorkingDaysTextView,PerDayCostTextView,CalculatedTotalAmountTextView,TotalQuantityForMonthTextView;
    private ExpandableHeightGridView calendarGridView;
    private Button addEventButton;
    ReminderDetails reminderDetails;
    private static final int MAX_CALENDAR_COLUMN = 42;
    private int month, year;
    checkCalenderdata updatenew  = this;
    List<NotificationDataTable> reminderDetailList;
    private SimpleDateFormat formatter = new SimpleDateFormat("MMMM yyyy", Locale.ENGLISH);
    private Calendar cal = Calendar.getInstance(Locale.ENGLISH);
    private Context context;
   // ImageButton imageButtoncheck,imageButtonCancel;
    private CalenderViewAdapter mAdapter; /*  calendarGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //Toast.makeText(CalenderView.this, "Clicked " + position, Toast.LENGTH_LONG).show();

            }
        });*/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calenderview);

        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        reminderDetails = (ReminderDetails) getIntent().getSerializableExtra("reminderDetails");
        reminderDetailList = new AddTaskActivityController().viewCalendar(this,reminderDetails, Getters.getMonthMMMyearFormat(this,cal.getTime()));
        toolbar.setTitle(reminderDetails.getReminderName());
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        previousButton = (ImageView) findViewById(R.id.previous_month);
        nextButton = (ImageView) findViewById(R.id.next_month);
        currentDate = (TextView) findViewById(R.id.display_current_date);
        TotalWorkingDaysTextView=(TextView)findViewById(R.id.txttotalworkingDay);
        PerDayCostTextView=(TextView)findViewById(R.id.txtperdayamount);
        CalculatedTotalAmountTextView=(TextView)findViewById(R.id.txtcalculteamount);
        TotalQuantityForMonthTextView=(TextView)findViewById(R.id.total_quantity_per_month);
        calendarGridView = (ExpandableHeightGridView) findViewById(R.id.calendar_grid);
        context=this;

        setUpCalendarAdapter();
        previousButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                cal.add(Calendar.MONTH, -1);
                setUpCalendarAdapter();
            }
        });
        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cal.add(Calendar.MONTH, 1);
                setUpCalendarAdapter();
            }
        });

    }
    private void setUpCalendarAdapter(){
        TotalWorkingDaysTextView.setText(context.getResources().getString(R.string.total_working_days)+" : 0");
        PerDayCostTextView.setText(context.getResources().getString(R.string.per_day_amount)+" : 0");
        CalculatedTotalAmountTextView.setText(context.getResources().getString(R.string.calculate_amount)+" : 0");
        List<Date> dayValueInCells = new ArrayList<Date>();
        Calendar mCal = (Calendar)cal.clone();
        reminderDetailList = new AddTaskActivityController().viewCalendar(this,reminderDetails, Getters.getMonthMMMyearFormat(this,mCal.getTime()));
        mCal.set(Calendar.DAY_OF_MONTH, 1);
        int firstDayOfTheMonth = mCal.get(Calendar.DAY_OF_WEEK) - 1;
        mCal.add(Calendar.DAY_OF_MONTH, -firstDayOfTheMonth);
        while(dayValueInCells.size() < MAX_CALENDAR_COLUMN){
            dayValueInCells.add(mCal.getTime());
            mCal.add(Calendar.DAY_OF_MONTH, 1);
        }
        Log.d(TAG, "Number of date " + dayValueInCells.size());
        String sDate = formatter.format(cal.getTime());
        currentDate.setText(sDate);
        mAdapter = new CalenderViewAdapter(CalenderView.this,R.layout.single_gride,dayValueInCells, cal,reminderDetailList,reminderDetails,updatenew);
        calendarGridView.setExpanded(true);
        calendarGridView.setAdapter(mAdapter);
        setTextForTextViews(reminderDetailList);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                this.finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
    public void setTextForTextViews(List<NotificationDataTable> reminderDetailListLocal){
       int totalmarkeddays =  reminderDetailListLocal.size();
       int totalDayswhenVendorIsPresent=0,totalDayswhenVendorIsAbsent=0;
       double perDayCost=0,totalQuantityForMonth=0;
       double totalMonthlyPay=0;
      if(reminderDetailListLocal!=null&&reminderDetailListLocal.size()>0) {

          for (NotificationDataTable userResponse : reminderDetailListLocal
                  ) {
              if (userResponse.isUserResponseOnNotification()) {
                  ++totalDayswhenVendorIsPresent;
                  perDayCost=userResponse.getPerTaskCost();
                  totalMonthlyPay+=perDayCost;
                  totalQuantityForMonth+=userResponse.getUnit();
              } else {
                  ++totalDayswhenVendorIsAbsent;
              }
          }

        //only if perdayCost is same for every object in NotificationDataTableList
        TotalWorkingDaysTextView.setText(context.getResources().getString(R.string.total_working_days)+" : "+totalDayswhenVendorIsPresent);
        PerDayCostTextView.setText(context.getResources().getString(R.string.per_day_amount)+" : "+String.format("%.2f",perDayCost));
        CalculatedTotalAmountTextView.setText(context.getResources().getString(R.string.calculate_amount)+" : "+String.format("%.2f",totalMonthlyPay));
        TotalQuantityForMonthTextView.setText(context.getResources().getString(R.string.total_quantity_for_month)+" : "+String.format("%.2f",totalQuantityForMonth));
      }
    }


    @Override
    public void onItemchange(Boolean result) {
        if(result.equals(true))
        {
          setUpCalendarAdapter();
        }

    }
}