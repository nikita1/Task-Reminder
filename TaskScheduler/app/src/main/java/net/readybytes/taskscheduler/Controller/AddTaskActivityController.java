package net.readybytes.taskscheduler.Controller;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteException;
import android.util.Log;
import android.widget.ListAdapter;
import android.widget.Toast;

import net.readybytes.taskscheduler.model.NotificationDataTable;
import net.readybytes.taskscheduler.model.ReminderDetails;
import net.readybytes.taskscheduler.view.AddTaskActivity;
import net.readybytes.taskscheduler.view.TaskDetail;

import java.util.List;

/**
 * Created by siddharh on 24/8/17.
 */

public class AddTaskActivityController {

    public void editparticularTask(ReminderDetails reminderDetails, String Action, Context context) {

        if (Action.equalsIgnoreCase("edit")) {
            Intent intent = new Intent(context, TaskDetail.class);
            intent.putExtra("ReminderDetailObject", reminderDetails);
            ((Activity)context).startActivityForResult(intent,100);


        } else if (Action.equalsIgnoreCase("delete")) {
            if (reminderDetails.getReminderId() != null) {
                try {
                    new DatabaseHandling(context).deleteTask(reminderDetails);
                    if (new AlarmManagerController().cancelSpecifiedAlarm(reminderDetails, context)) {
                        Toast.makeText(context, "ALARM CANCELLED", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(context, "ReminderDetails  Id  is null", Toast.LENGTH_SHORT).show();
                    }
                } catch (SQLiteException e) {
                    Toast.makeText(context, "Failed to delete table", Toast.LENGTH_SHORT).show();
                }
            }

        }
    }
    public List<NotificationDataTable> viewCalendar(Context context,ReminderDetails reminderDetails,String monthYear){

        return  new DatabaseHandling(context).getListofNotificationData(context,reminderDetails,monthYear);

    }




}
