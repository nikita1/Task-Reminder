package net.readybytes.taskscheduler.view;

import android.app.AlarmManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import net.readybytes.taskscheduler.Controller.AlarmManagerController;
import net.readybytes.taskscheduler.Controller.DatabaseHandling;
import net.readybytes.taskscheduler.Controller.LaunchTimePicker;
import net.readybytes.taskscheduler.Controller.ReminderCreationController;
import net.readybytes.taskscheduler.Controller.ViewController;
import net.readybytes.taskscheduler.R;
import net.readybytes.taskscheduler.globalusage.Getters;
import net.readybytes.taskscheduler.model.ReminderDetails;

import java.io.ByteArrayOutputStream;
import java.sql.Time;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

public class TaskDetail extends AppCompatActivity implements View.OnClickListener {
    EditText taskNameEditText, ReminderTimeSetter, ReminderDateSetter, VendorNameEditText, VendorNumberEditText, perDayCostEdittext,QuantityEdittext;
    Spinner RepeatSpinnerOption, WagesPerDayOrMonth;
    ImageView clockimageview, calenderImageiew;
    Button btnUpdate,btnCancel;
    Time time;
    LinearLayout QuantityEdittextlinearLayout,checkboxwithTextviewLinearLayout;
    CheckBox QuantityCheckBox;
    Date date;
    double perDayCost,CostPerQuantity;
    AlarmManager alarmManager;
    ReminderDetails reminderDetails;
    double Quantity=1;
    Calendar calendar;
    Context context;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        TextView txtrepeatNoofTime;
        setContentView(R.layout.activity_task_detail);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        taskNameEditText = (EditText) findViewById(R.id.taskNameeditText);
        ReminderTimeSetter = (EditText) findViewById(R.id.reminderTimeEditText);
        ReminderDateSetter = (EditText) findViewById(R.id.reminderDateEditText);
        VendorNameEditText = (EditText) findViewById(R.id.venderNameEdittext);
        VendorNumberEditText = (EditText) findViewById(R.id.venderMobileNumberEdittext);
        RepeatSpinnerOption = (Spinner) findViewById(R.id.repeatOptionSpinner);
        btnUpdate = (Button) findViewById(R.id.donebutton);
        btnCancel = (Button)findViewById(R.id.btncancel);
        btnUpdate.setOnClickListener(this);
        btnCancel.setOnClickListener(this);
        context= TaskDetail.this;
        calendar = Calendar.getInstance();
        alarmManager=(AlarmManager)getSystemService(ALARM_SERVICE);
        clockimageview=(ImageView)findViewById(R.id.reminderTimeImageView);
        calenderImageiew=(ImageView)findViewById(R.id.reminderDateImageView);
        perDayCostEdittext = (EditText) findViewById(R.id.venderPerDayPriceEdittext);
        QuantityEdittext   = (EditText) findViewById(R.id.QuantityEdittext);
        QuantityEdittextlinearLayout=(LinearLayout)findViewById(R.id.QuantityEdittextLinearLayout);
        checkboxwithTextviewLinearLayout=(LinearLayout)findViewById(R.id.checkboxwithTextviewLinearLayout);
        QuantityCheckBox   =(CheckBox)findViewById(R.id.quantityCheckbox);
        WagesPerDayOrMonth=(Spinner)findViewById(R.id.wagesPerDayOrMonthSpinner);
        reminderDetails = (ReminderDetails) getIntent().getSerializableExtra("ReminderDetailObject");
        toolbar.setTitle(reminderDetails.getReminderName());
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        setValues();
        if(reminderDetails.getUnit()!=0){
            QuantityCheckBox.setChecked(true);
            QuantityEdittext.setText(String.valueOf(reminderDetails.getUnit()));
        }
        if(!QuantityCheckBox.isChecked()){
            QuantityEdittextlinearLayout.setVisibility(View.GONE);
        }
        QuantityCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if(isChecked){
                    QuantityEdittextlinearLayout.setVisibility(View.VISIBLE);
                }else{
                    QuantityEdittext.getText().clear();
                    QuantityEdittextlinearLayout.setVisibility(View.GONE);
                }
            }
        });
        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(this, R.layout.spinner_item,
                getResources().getStringArray(R.array.repeatItem))
        {
            @Override
            public boolean isEnabled(int position){
                if(position == 0)
                {
                    // Disable the first item from Spinner
                    // First item will be use for hint
                    return false;
                }
                else
                {
                    return true;
                }
            }
            @Override
            public View getDropDownView(int position, View convertView,
                                        ViewGroup parent)
            {
                View view = super.getDropDownView(position, convertView, parent);
                TextView tv = (TextView) view;
                if(position == 0){
                    // Set the hint text color gray
                    tv.setTextColor(Color.GRAY);
                }
                else {
                    tv.setTextColor(Color.BLACK);
                }
                return view;
            }
        };

        spinnerArrayAdapter.setDropDownViewResource(R.layout.spinner_item);
        RepeatSpinnerOption.setAdapter(spinnerArrayAdapter);
        int position = spinnerArrayAdapter.getPosition(reminderDetails.getRemindertimePer());
        RepeatSpinnerOption.setSelection(position);
        RepeatSpinnerOption.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if(RepeatSpinnerOption.getSelectedItem().toString().equalsIgnoreCase("monthly")){
                    QuantityCheckBox.setChecked(false);
                    checkboxwithTextviewLinearLayout.setVisibility(View.GONE);
                }
              else{
                    checkboxwithTextviewLinearLayout.setVisibility(View.VISIBLE);
                }
            }



            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
      });
        ArrayAdapter<String> spinnerArrayAdapterByCost = new ArrayAdapter<String>(this, R.layout.spinner_item,
                getResources().getStringArray(R.array.costBy));

        WagesPerDayOrMonth.setAdapter(spinnerArrayAdapterByCost);
        int costItemSelectposition= spinnerArrayAdapterByCost.getPosition(reminderDetails.getWagesPerDayorMonth());
        WagesPerDayOrMonth.setSelection(costItemSelectposition);

        ReminderTimeSetter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new LaunchTimePicker().launchTimePicker(TaskDetail.this,ReminderTimeSetter);
            }
        });
        ReminderDateSetter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new LaunchTimePicker().launchDatePicker(TaskDetail.this,ReminderDateSetter,calendar);
            }
        });
        clockimageview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new LaunchTimePicker().launchTimePicker(TaskDetail.this,ReminderTimeSetter);
            }
        });
        calenderImageiew.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new LaunchTimePicker().launchDatePicker(TaskDetail.this,ReminderDateSetter,calendar);
            }
        });

    }

    public void setValues() {
        taskNameEditText.setText(reminderDetails.getReminderName());
        ReminderTimeSetter.setText(Getters.getStringwhenInput24hrs(context,reminderDetails.getReminderTime().toString()));
        ReminderDateSetter.setText(Getters.getdateStringddMMyyyy(context,reminderDetails.getReminderDate()));
        VendorNameEditText.setText(reminderDetails.getServicemanName());
        VendorNumberEditText.setText(reminderDetails.getServicemanNumber());
        perDayCostEdittext.setText(String.valueOf(reminderDetails.getCostPerUnit()));


    }

    public ReminderDetails sendingValuesforUpdate() {
        Bitmap largeIcon = BitmapFactory.decodeResource(getResources(), R.drawable.thumbsup);
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        largeIcon.compress(Bitmap.CompressFormat.JPEG, 100, stream);
        byte[] byteArray = stream.toByteArray();
        time = Getters.getTimeObjectwhenInputAMPM(context,ReminderTimeSetter.getText().toString());
        date = Getters.getdateasDateObject(context,ReminderDateSetter.getText().toString());
        //to clear QuantityEdittext if Checkbox is not checked
        if(!QuantityCheckBox.isChecked()){
            QuantityEdittext.getText().clear();
        }
        if(QuantityEdittext!=null&&QuantityEdittext.getVisibility()==View.VISIBLE&&!QuantityEdittext.getText().toString().equalsIgnoreCase("")) {
        Quantity = Double.parseDouble(String.format("%.2f",Double.parseDouble(QuantityEdittext.getText().toString())));
        }
        CostPerQuantity=Double.parseDouble(String.format("%.2f",Double.parseDouble(perDayCostEdittext.getText().toString())));
        perDayCost=Quantity*CostPerQuantity;
        if(WagesPerDayOrMonth.getSelectedItem().toString().equalsIgnoreCase("month")){
            //dividing the whole amount by number of days,and assuming it ideally as 30;
            perDayCost/=30;
        }

        ReminderDetails reminderDetailsNew = new ReminderDetails(taskNameEditText.getText().toString(),
                true,
                VendorNameEditText.getText().toString(),
                VendorNumberEditText.getText().toString(),
                byteArray,
                RepeatSpinnerOption.getSelectedItem().toString(),
                time,
                date,
                Double.parseDouble(String.format("%.2f",perDayCost)),
                1,
                reminderDetails.getRequestCode(),
                Quantity,
                CostPerQuantity,
                WagesPerDayOrMonth.getSelectedItem().toString());
        reminderDetailsNew.setReminderId(reminderDetails.getReminderId());
        return reminderDetailsNew;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.donebutton:
                if(validation()) {
                        ReminderDetails reminderDetails =sendingValuesforUpdate();
                    if (new DatabaseHandling(getBaseContext()).update(reminderDetails.isReminderActive(), reminderDetails, "edit") > 0) {
                        long interval = new ReminderCreationController().calculateinterval(reminderDetails.getRemindertimePer().toString(), calendar, this);
                        if(new AlarmManagerController().startalarmManager(context, reminderDetails, reminderDetails.getRequestCode(), alarmManager, time, date, interval, context.getResources().getString(R.string.action_update))){
                            Toast.makeText(this,getResources().getString(R.string.UpdatedandAlarmUpdated), Toast.LENGTH_SHORT).show();
                        }
                      //  new ViewController().launchView("homescreen", getBaseContext());
                        Intent intent = getIntent();
                        setResult(100, intent);
                        this.finish();

                    }
                }
            break;
            case R.id.btncancel:
                this.finish();
        }
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                this.finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public boolean validation(){
        String taskname                     =taskNameEditText.getText().toString();
        String Vendorname                   =VendorNameEditText.getText().toString();
        String VendorNumber                 =VendorNumberEditText.getText().toString();
        String CostPerQuantity              =perDayCostEdittext.getText().toString();
        String SpinnerWagesPerDayOrMonth    =WagesPerDayOrMonth.getSelectedItem().toString();
        Boolean taskalreadycreated          =false;
        if (taskname.equals(""))
        {
            taskNameEditText.setError(this.getResources().getString(R.string.validation_TaskNameEmpty));
            return false;
        }
        else
        {
            List<ReminderDetails> list = new DatabaseHandling(context).getListofallClients();
            for (ReminderDetails node:
                    list)
            {
                if(taskname.equals(node.getReminderName())){
                    if(reminderDetails.getReminderId().equalsIgnoreCase(node.getReminderId())){
                    taskalreadycreated=false;
                        break;
                    }
                    taskalreadycreated=true;
                    break;
                }
            }
            if(taskalreadycreated)
            {
                taskNameEditText.setError(this.getResources().getString(R.string.validation_sametask_error));

                return false;
            }

        }
        if(RepeatSpinnerOption.getSelectedItem().equals(getResources().getString(R.string.selectrepeatinterval)))
        {
            View selectedView = RepeatSpinnerOption.getSelectedView();
            if (selectedView != null && selectedView instanceof TextView)
            {
                TextView errorText = (TextView)RepeatSpinnerOption.getSelectedView();
                errorText.setError("");
                errorText.setTextColor(Color.RED);//just to highlight that this is an error
                errorText.setText(getResources().getString(R.string.error_repeatinterval));
            }
            return false;
        }
        if(Vendorname.equalsIgnoreCase("")){
            VendorNameEditText.setError(this.getResources().getString(R.string.vendorNameEmpty));
            return false;
        }
        if(VendorNumber.equalsIgnoreCase("")){
            VendorNumberEditText.setError(this.getResources().getString(R.string.vendorNumberEmpty));
            return false;
        }
        if(CostPerQuantity.equalsIgnoreCase("")){
            perDayCostEdittext.setError(this.getResources().getString(R.string.CostPerQuantityEmpty));
            return false;
        }
        return true;
    }
}



