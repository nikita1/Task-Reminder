package net.readybytes.taskscheduler.broadcastrecievers;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import net.readybytes.taskscheduler.Controller.DatabaseHandling;
import net.readybytes.taskscheduler.R;
import net.readybytes.taskscheduler.SessionManager;
import net.readybytes.taskscheduler.model.ReminderDetails;

import java.sql.Time;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static android.content.Context.ALARM_SERVICE;

public class LaunchAlarmsAgain extends BroadcastReceiver {
    private PendingIntent pendingIntent;
    SessionManager sessionManager;
    @Override
    public void onReceive(Context context, Intent intent) {
        // TODO: This method is called when the BroadcastReceiver is receiving
        // an Intent broadcast.
        Log.d("LaunchAlarmsAgain","1");
        Time time;
        Date date;
        sessionManager =  new SessionManager(context);
        SimpleDateFormat date1 = new SimpleDateFormat("yyyy/MM/dd");
        SimpleDateFormat date2 = new SimpleDateFormat("HH:mm");
        String date3,date4;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm");
        AlarmManager alarmManager;
        alarmManager=(AlarmManager)context.getSystemService(ALARM_SERVICE) ;
        Log.d("LaunchAlarmsAgain","1");
        List<ReminderDetails> reminderDetailsList =  new DatabaseHandling(context).getListofallClients();
        for (ReminderDetails reminderDetails :reminderDetailsList) {
/*
           date = reminderDetails.getReminderDate();
           time = reminderDetails.getReminderTime();
           date3 = date1.format(date);
            date4 = date2.format(time);
            String myDate = date3 + " " + date4;*/
            try {
                if(reminderDetails.isReminderActive()) {
                    if (sessionManager.getLastDate(reminderDetails.getReminderName()) != null) {
                        date = sessionManager.getLastDate(reminderDetails.getReminderName());
                    } else {
                        date = new Date();
                    }
                    Intent myIntent = new Intent(context, AlarmReciever.class);
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("ReminderDetails",reminderDetails);
                    bundle.putLong("requestCode", reminderDetails.getRequestCode());
                    myIntent.putExtra("RequestBundle",bundle);

                    long interval = calculateinterval(reminderDetails.getRemindertimePer(), context);
                    launchAlarmwithSpecifiedIntents(myIntent, context, reminderDetails.getRequestCode(), alarmManager, date, interval, "update");
                }else {
                    Log.d("LaunchAlarmsAgain","Alarm disable");
                }
            Log.d("LaunchAlarmsAgain","2");
            } catch (NullPointerException e) {
                Log.d("LaunAlarmsAgain","ENDS INTO NULLPOINTER EXCEPTION");
                e.printStackTrace();
            }
        }

    }
    public void launchAlarmwithSpecifiedIntents(Intent myIntent, Context context, long requestCode, AlarmManager alarmManager, Date currentDate, long interval, String action) {
        if (interval != 0) {
            if (action.equals("create")) {
                pendingIntent = PendingIntent.getBroadcast(context, (int) requestCode, myIntent, PendingIntent.FLAG_UPDATE_CURRENT);
            }
            if (action.equals("update")) {
                pendingIntent = PendingIntent.getBroadcast(context, (int) requestCode, myIntent, PendingIntent.FLAG_CANCEL_CURRENT);
            }
            Log.d("LaunchAlarmsAgain", "3");
            alarmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP, currentDate.getTime(),
                    interval, pendingIntent);
        }else if(interval==0){
            alarmManager.set(AlarmManager.RTC_WAKEUP, currentDate.getTime(),
                        pendingIntent);
        }
    }
    public long calculateinterval(String interval, Context context){
        long intervallong=0;
        if(interval.equals(context.getResources().getString(R.string.daily))){
            intervallong= AlarmManager.INTERVAL_DAY;
        }else if(interval.equals(context.getResources().getString(R.string.monthly))) {
            Calendar c = Calendar.getInstance();
            int monthMaxDays = c.getActualMaximum(Calendar.DAY_OF_MONTH);
            intervallong = AlarmManager.INTERVAL_DAY * monthMaxDays;
        }
        return intervallong;
    }

}
