package net.readybytes.taskscheduler.adapter;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.OperationApplicationException;
import android.graphics.Point;
import android.icu.lang.UCharacter;
import android.media.MediaRouter;
import android.preference.DialogPreference;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import net.readybytes.taskscheduler.Controller.AddTaskActivityController;
import net.readybytes.taskscheduler.Controller.NotificationDataController;
import net.readybytes.taskscheduler.Controller.ViewController;
import net.readybytes.taskscheduler.R;
import net.readybytes.taskscheduler.check_update_data;
import net.readybytes.taskscheduler.globalusage.Getters;
import net.readybytes.taskscheduler.model.NotificationDataTable;
import net.readybytes.taskscheduler.model.ReminderDetails;
import net.readybytes.taskscheduler.view.CalenderView;
import net.readybytes.taskscheduler.view.checkCalenderdata;

import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by rbsl on 2/9/17.
 */

public class CalenderViewAdapter extends BaseAdapter {
    private static final String TAG = CalenderViewAdapter.class.getSimpleName();
    Context context;
    int layoutResourceId;
    static int dayvalueNonfinal,currentMonth,currentYear;
    static int positionForCalender;
    private List<Date> monthlyDates;
    private Calendar currentDate;
    static String action;
    static NotificationDataTable notificationDataTable;
    static LinkedList<Boolean> userResponse;
    List<NotificationDataTable> EventList;
    checkCalenderdata checkCalenderdata;
    ReminderDetails reminderDetails;
    boolean userResponseRadioButtonYesOrNo;
    //private List<EventObjects> allEvents;
    double costPerday,costPerUnit;
    double Unit;
    TextView HintWhereUnitEqualsTextView;
    public CalenderViewAdapter(Context c, int layoutResourceId, List<Date> monthlyDates, Calendar currentDate, List<NotificationDataTable> eventList,ReminderDetails reminderDetails,checkCalenderdata checkCalenderdata) {
        context = c;
        this.reminderDetails  = reminderDetails;
        this.layoutResourceId = layoutResourceId;
        this.monthlyDates     = monthlyDates;
        this.currentDate      = currentDate;
        this.EventList        = eventList;
        this.checkCalenderdata = checkCalenderdata;
    }




    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return monthlyDates.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public View getView(final int position, final View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        View v;
        UserHolder holder = null;
      /*  LayoutInflater inflater = (LayoutInflater) mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);*/

        if (convertView == null) {

            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            v = inflater.inflate(layoutResourceId, parent, false);
        }

            else {
                v = (View) convertView;
            }

            holder = new UserHolder();
            holder.txtstorename = (TextView)v.findViewById(R.id.calendar_date_id);
            //holder.txttickmark = (TextView)row.findViewById(R.id.event_id);
            holder.imageViewforChecked=(ImageView)v.findViewById(R.id.event_id_Ok);
            holder.storeGride = (LinearLayout)v.findViewById(R.id.calenderGrid);
            Date mDate = monthlyDates.get(position);
            final Calendar dateCal = Calendar.getInstance();
            dateCal.setTime(mDate);
            final int dayValue = dateCal.get(Calendar.DAY_OF_MONTH);
            int displayMonth = dateCal.get(Calendar.MONTH) + 1;
            int displayYear = dateCal.get(Calendar.YEAR);
            currentMonth = currentDate.get(Calendar.MONTH) + 1;
            currentYear = currentDate.get(Calendar.YEAR);
            if(displayMonth == currentMonth && displayYear == currentYear) {
                if (EventList != null && EventList.size() != 0) {
                    for (int a = 0; a < EventList.size(); a++) {
                        if (dayValue == EventList.get(a).getReminderdateNotificationData().getDate()) {
                            holder.NotificationDataTableObject = EventList.get(a);
                            if (EventList.get(a).isUserResponseOnNotification()) {
                                /*holder.txttickmark.setBackgroundResource(R.color.toolbarcolor);*/
                                holder.imageViewforChecked.setVisibility(View.VISIBLE);
                                holder.imageViewforChecked.setBackgroundResource(R.drawable.checked);
                            }else{/*
                                holder.txttickmark.setBackgroundResource(R.color.black);*/
                                holder.imageViewforChecked.setVisibility(View.VISIBLE);
                                holder.imageViewforChecked.setBackgroundResource(R.drawable.cancel);

                            }
                           // Toast.makeText(context, "hekfh", Toast.LENGTH_SHORT).show();
                        }
                    //
                    }


            }

                final UserHolder finalholder =  holder;

                holder.storeGride.setClickable(true);

               // holder.storeGride.setBackgroundResource(R.drawable.rimpleeffect);
                holder.txtstorename.setText(String.valueOf(dayValue));

                holder.storeGride.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Log.d("value_is", String.valueOf(dayValue));
                        try {
                            notificationDataTable = finalholder.NotificationDataTableObject;
                            /**
                             * Cost per Day ,Cost per Unit has been assigned,this storeGrid CLick listener takes all different values for each notificationDataTable Object mentioned in EventList,
                             * After that Update and Insert is assigned to the appropriate acc
                             */
                            costPerUnit = reminderDetails.getCostPerUnit();
                            costPerday = reminderDetails.getCostPerDay();
                            if (finalholder.NotificationDataTableObject != null) {
                                if (finalholder.imageViewforChecked.getVisibility() == View.VISIBLE) {
                                    action = "update";
                                } else {
                                    action = "insert";
                                }
                            } else if (finalholder.NotificationDataTableObject == null) {
                                action = "insertNew";
                            }
                            if (reminderDetails.getWagesPerDayorMonth().equalsIgnoreCase("day")&&reminderDetails.getRemindertimePer().equalsIgnoreCase("daily")) {
                                final Dialog openDialog = new Dialog(context);
                                openDialog.setContentView(R.layout.activity_calendar_popup);
                                openDialog.setTitle("Custom Dialog Box");
                                Button btnCancel = (Button) openDialog.findViewById(R.id.btncancle);
                                final RadioGroup radioGroup = (RadioGroup) openDialog.findViewById(R.id.radiogroup);
                                Button btndone = (Button) openDialog.findViewById(R.id.btndone);
                                final EditText UnitEditText = (EditText) openDialog.findViewById(R.id.editQuantity);
                               // HintWhereUnitEqualsTextView = (TextView) openDialog.findViewById(R.id.hintWhereUnitEqualsTextview);
                                if(reminderDetails!=null&& UnitEditText!=null) {
                                    UnitEditText.setText(String.valueOf(reminderDetails.getUnit()));
                                }
                                if(notificationDataTable!=null&&notificationDataTable.getUnit()!=0){
                                    UnitEditText.setText(String.valueOf(notificationDataTable.getUnit()));
                                }
                                btndone.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        RadioButton radioButton = (RadioButton) openDialog.findViewById(radioGroup.getCheckedRadioButtonId());
                                        // TODO Auto-generated method stub
                                        if (radioButton.getText().equals("Yes")) {
                                            userResponseRadioButtonYesOrNo = true;
                                        } else {
                                            userResponseRadioButtonYesOrNo = false;
                                        }
                                        if (!(UnitEditText.getText().toString().equals(""))) {
                                            Unit = Double.parseDouble(UnitEditText.getText().toString());
                                        } else {
                                            //not filled any quantity,so by default we are filling Unit
                                            Unit = 1.0;
                                        }
                                        costPerday=(Unit*costPerUnit);
                                        Date datewithTime00_00_00 = Getters.getdateasDateObject(context, (dayvalueNonfinal + "/" + currentMonth + "/" + currentYear));
                                        NotificationDataTable notificationDataTableLocalScope = new NotificationDataTable(reminderDetails.getReminderId(), reminderDetails.getReminderName(), reminderDetails.getReminderTime(), datewithTime00_00_00, costPerday, userResponseRadioButtonYesOrNo, Unit, costPerUnit, reminderDetails.getWagesPerDayorMonth());
                                        if (action.equals("insertNew")) {
                                            new NotificationDataController().createTableOnEvent(context, notificationDataTableLocalScope, reminderDetails, action);
                                        } else if (action.equals("insert") || action.equals("update")) {
                                            notificationDataTableLocalScope.setNotificationUniqueId_perTask(notificationDataTable.getNotificationUniqueId_perTask());
                                            new NotificationDataController().createTableOnEvent(context, notificationDataTableLocalScope, reminderDetails, action);
                                        }
                                        checkCalenderdata.onItemchange(true);
                                        openDialog.dismiss();
                                    }
                                });
                                btnCancel.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        openDialog.dismiss();
                                    }
                                });
                                openDialog.show();

                            /*final AlertDialog.Builder builderForOptions = new AlertDialog.Builder(context);
                            builderForOptions.setTitle("Options");
                            builderForOptions.setCancelable(true);
                            builderForOptions.setNeutralButton(context.getResources().getString(R.string.calenderdetailpopup), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    final AlertDialog.Builder builder = new AlertDialog.Builder(context);
                                     builder.setTitle("Update Task Reminder");
                                    builder.setCancelable(true);
                                    builder.setMessage(context.getResources().getString(R.string.calenderdetailpopup));
                                    builder.setNeutralButton(context.getResources().getString(R.string.cancle), new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            dialogInterface.dismiss();

                                        }
                                    });
                                    builder.setPositiveButton(context.getResources().getString(R.string.yes), new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            try {
                                                Date datewithTime00_00_00 = Getters.getdateasDateObject(context, (dayvalueNonfinal + "/" + currentMonth + "/" + currentYear));
                                                NotificationDataTable notificationDataTableLocalScope = new NotificationDataTable(reminderDetails.getReminderId(), reminderDetails.getReminderName(), reminderDetails.getReminderTime(), datewithTime00_00_00, costPerday, true, Unit, costPerUnit, reminderDetails.getWagesPerDayorMonth());
                                                if (action.equals("insertNew")) {
                                                    new NotificationDataController().createTableOnEvent(context, notificationDataTableLocalScope, reminderDetails, action);
                                                } else if (action.equals("insert") || action.equals("update")) {
                                                    notificationDataTableLocalScope.setNotificationUniqueId_perTask(notificationDataTable.getNotificationUniqueId_perTask());
                                                    new NotificationDataController().createTableOnEvent(context, notificationDataTableLocalScope, reminderDetails, action);
                                                }

                                            } catch (NumberFormatException e) {
                                                Toast.makeText(context, R.string.Exception_NoDoubleValue, Toast.LENGTH_SHORT).show();
                                            }
                                            checkCalenderdata.onItemchange(true);
                                        }
                                    })
                                            .setNegativeButton(context.getResources().getString(R.string.no), new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int which) {
                                                    try {
                                                        Date datewithTime00_00_00 = Getters.getdateasDateObject(context, (dayvalueNonfinal + "/" + currentMonth + "/" + currentYear));
                                                        NotificationDataTable notificationDataTableLocalScope = new NotificationDataTable(reminderDetails.getReminderId(), reminderDetails.getReminderName(), reminderDetails.getReminderTime(), datewithTime00_00_00, costPerday, false, Unit, costPerUnit, reminderDetails.getWagesPerDayorMonth());
                                                        if (action.equals("insertNew")) {
                                                            new NotificationDataController().createTableOnEvent(context, notificationDataTableLocalScope, reminderDetails, action);
                                                        } else if (action.equals("insert") || action.equals("update")) {
                                                            notificationDataTableLocalScope.setNotificationUniqueId_perTask(notificationDataTable.getNotificationUniqueId_perTask());
                                                            new NotificationDataController().createTableOnEvent(context, notificationDataTableLocalScope, reminderDetails, action);
                                                        }
                                                    } catch (NumberFormatException e) {
                                                        Toast.makeText(context, R.string.Exception_NoDoubleValue, Toast.LENGTH_SHORT).show();
                                                    }
                                                    checkCalenderdata.onItemchange(true);
                                                }
                                            });
                                    builder.show();
                                }
                            });
                            builderForOptions.show();*/
                            } else {
                                final AlertDialog.Builder builderForOptions = new AlertDialog.Builder(context);
                                builderForOptions.setCancelable(true);
                              builderForOptions.setMessage(context.getResources().getString(R.string.notification_message));
                                builderForOptions.setPositiveButton(context.getResources().getString(R.string.yes), new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        try {
                                            Date datewithTime00_00_00 = Getters.getdateasDateObject(context, (dayvalueNonfinal + "/" + currentMonth + "/" + currentYear));
                                            NotificationDataTable notificationDataTableLocalScope = new NotificationDataTable(reminderDetails.getReminderId(), reminderDetails.getReminderName(), reminderDetails.getReminderTime(), datewithTime00_00_00, costPerday, true, Unit, costPerUnit, reminderDetails.getWagesPerDayorMonth());
                                            if (action.equals("insertNew")) {
                                                new NotificationDataController().createTableOnEvent(context, notificationDataTableLocalScope, reminderDetails, action);
                                            } else if (action.equals("insert") || action.equals("update")) {
                                                notificationDataTableLocalScope.setNotificationUniqueId_perTask(notificationDataTable.getNotificationUniqueId_perTask());
                                                new NotificationDataController().createTableOnEvent(context, notificationDataTableLocalScope, reminderDetails, action);
                                            }

                                        } catch (NumberFormatException e) {
                                            Toast.makeText(context, R.string.Exception_NoDoubleValue, Toast.LENGTH_SHORT).show();
                                        }
                                        checkCalenderdata.onItemchange(true);
                                    }
                                })
                                        .setNegativeButton(context.getResources().getString(R.string.no), new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int which) {
                                                try {
                                                    Date datewithTime00_00_00 = Getters.getdateasDateObject(context, (dayvalueNonfinal + "/" + currentMonth + "/" + currentYear));
                                                    NotificationDataTable notificationDataTableLocalScope = new NotificationDataTable(reminderDetails.getReminderId(), reminderDetails.getReminderName(), reminderDetails.getReminderTime(), datewithTime00_00_00, costPerday, false, Unit, costPerUnit, reminderDetails.getWagesPerDayorMonth());
                                                    if (action.equals("insertNew")) {
                                                        new NotificationDataController().createTableOnEvent(context, notificationDataTableLocalScope, reminderDetails, action);
                                                    } else if (action.equals("insert") || action.equals("update")) {
                                                        notificationDataTableLocalScope.setNotificationUniqueId_perTask(notificationDataTable.getNotificationUniqueId_perTask());
                                                        new NotificationDataController().createTableOnEvent(context, notificationDataTableLocalScope, reminderDetails, action);
                                                    }
                                                } catch (NumberFormatException e) {
                                                    Toast.makeText(context, R.string.Exception_NoDoubleValue, Toast.LENGTH_SHORT).show();
                                                }
                                                checkCalenderdata.onItemchange(true);
                                            }
                                        });
                                builderForOptions.show();
                            }
                            positionForCalender = reminderDetails.getReminderDate().getDate();
                            dayvalueNonfinal = dayValue;
                            //Toast.makeText(context,String.valueOf(dayValue), Toast.LENGTH_SHORT).show();
                        }catch (NumberFormatException e){
                            Toast.makeText(context, "Could Not parse false Values which were entered", Toast.LENGTH_SHORT).show();
                        }
                    }
                });


            }




            v.setTag(holder);



/*

        Date mDate = monthlyDates.get(position);
        Calendar dateCal = Calendar.getInstance();
        dateCal.setTime(mDate);
        final int dayValue = dateCal.get(Calendar.DAY_OF_MONTH);
        int displayMonth = dateCal.get(Calendar.MONTH) + 1;
        int displayYear = dateCal.get(Calendar.YEAR);
        int currentMonth = currentDate.get(Calendar.MONTH) + 1;
        int currentYear = currentDate.get(Calendar.YEAR);
        if(displayMonth == currentMonth && displayYear == currentYear) {
            holder.txtstorename.setText(dayValue);
        }
*/


        return v;
    }




    static class UserHolder {


        TextView txtstorename;
        TextView txttickmark;
        ImageView imageViewforChecked;
        LinearLayout storeGride;
        NotificationDataTable NotificationDataTableObject;

    }

}