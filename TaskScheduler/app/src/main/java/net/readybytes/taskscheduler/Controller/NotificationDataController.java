package net.readybytes.taskscheduler.Controller;

import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import net.readybytes.taskscheduler.TaskInitilizerFragment;
import net.readybytes.taskscheduler.model.NotificationDataTable;
import net.readybytes.taskscheduler.model.ReminderDetails;

import java.sql.Time;
import java.util.Date;

import static android.content.Context.NOTIFICATION_SERVICE;

/**
 * Created by siddharh on 30/8/17.
 */

public class NotificationDataController {

    public void processNotification(Context context,Intent intent){
        Bundle answerBundle = intent.getExtras();
        String UserResponse="";
        int userAnswer = answerBundle.getInt("userAnswer");
        int requestCode = answerBundle.getInt("requestcode");
        NotificationManager notificationManager =
                (NotificationManager) context.getSystemService(NOTIFICATION_SERVICE);
        Log.d("notificationButton","No"+answerBundle.getInt("userAnswer"));
        ReminderDetails reminderDetails=(ReminderDetails)intent.getSerializableExtra("ReminderDetailObject");
        Date date = (Date) answerBundle.getSerializable("date");
        if(userAnswer == 1) {
            UserResponse=  "Yes";
            NotificationDataTable notificationDataTable = new NotificationDataTable(reminderDetails.getReminderId(),reminderDetails.getReminderName(),reminderDetails.getReminderTime(),date,reminderDetails.getCostPerDay(),true,reminderDetails.getUnit(),reminderDetails.getCostPerUnit(),reminderDetails.getWagesPerDayorMonth());
            createTableOnEvent(context,notificationDataTable,reminderDetails,"create");
            createTableOnEvent(context,notificationDataTable,reminderDetails,"insert");
        }else if(userAnswer == 0) {
            UserResponse= "No";
            NotificationDataTable notificationDataTable = new NotificationDataTable(reminderDetails.getReminderId(),reminderDetails.getReminderName(),reminderDetails.getReminderTime(),date,reminderDetails.getCostPerDay(),false, reminderDetails.getUnit(),reminderDetails.getCostPerUnit(),reminderDetails.getWagesPerDayorMonth());
            NotificationDataController notificationDataController=new NotificationDataController();
            createTableOnEvent(context,notificationDataTable,reminderDetails,"create");
            createTableOnEvent(context,notificationDataTable,reminderDetails,"insert");
        }
        Toast.makeText(context, "YUP!! Your Response as"+UserResponse+"have been recorded", Toast.LENGTH_SHORT).show();
            notificationManager.cancel(requestCode);
    }


    public void createTableOnEvent(Context context,NotificationDataTable notificationDataTable,ReminderDetails reminderDetails,String action){
   if(action.equals("create")) {
       if (context != null) {
           if (notificationDataTable.getRemindertimeNotificationData() != null && notificationDataTable.getReminderdateNotificationData() != null & reminderDetails != null) {
               new DatabaseHandling(context).createNotificationTable(notificationDataTable,reminderDetails);
               Log.d("NotificationData","created");
           }
       }
   }else if(action.equals("insert")||action.equals("insertNew")) {
       new DatabaseHandling(context).createNotificationTable(notificationDataTable,reminderDetails);
       new DatabaseHandling(context).insertNotificationTable(context,notificationDataTable,reminderDetails);
        Log.d("NotificationData","insert");
   }else if(action.equals("update")){
        new DatabaseHandling(context).updateNotificationTableForATask(notificationDataTable,reminderDetails,action);
        Toast.makeText(context, "Updated your calendar", Toast.LENGTH_SHORT).show();
   }
    }

}
