package net.readybytes.taskscheduler.adapter;

import android.Manifest;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Point;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import net.readybytes.taskscheduler.Controller.AddTaskActivityController;
import net.readybytes.taskscheduler.Controller.AlarmManagerController;
import net.readybytes.taskscheduler.Controller.DatabaseHandling;
import net.readybytes.taskscheduler.Controller.ReminderCreationController;
import net.readybytes.taskscheduler.Controller.ViewController;
import net.readybytes.taskscheduler.R;
import net.readybytes.taskscheduler.TaskInitilizerFragment;
import net.readybytes.taskscheduler.check_update_data;
import net.readybytes.taskscheduler.globalusage.Getters;
import net.readybytes.taskscheduler.model.NotificationDataTable;
import net.readybytes.taskscheduler.model.ReminderDetails;
import net.readybytes.taskscheduler.view.AddTaskActivity;
import net.readybytes.taskscheduler.view.CalenderView;
import net.readybytes.taskscheduler.view.TaskDetail;

import org.w3c.dom.Text;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static android.content.Context.ALARM_SERVICE;

/**
 * Created by rbsl on 26/8/17.
 */

public class TaskListAdapter extends ArrayAdapter<ReminderDetails> {
    Context context;
    int mHour, mMinute;
    static final String TAG = "inAdapter";
    String countryCode;
    int layoutResourceId;
    check_update_data update;
    String temp_country_code;
    ArrayList<ReminderDetails> data = new ArrayList<ReminderDetails>();

    public TaskListAdapter(Context context, int layoutResourceId, ArrayList<ReminderDetails> data, check_update_data updatenew) {
        super(context, layoutResourceId, data);
        this.layoutResourceId = layoutResourceId;
        this.context = context;
        this.data = data;
        this.update = updatenew;
    }

    public View getView(int position, final View convertView, ViewGroup parent) {
        View row = convertView;
        UserHolder holder = null;
        if (row == null) {
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);
            holder = new UserHolder();
            holder.txtReminderName = (TextView) row.findViewById(R.id.txtReminderName);
            holder.txtasktime = (TextView) row.findViewById(R.id.txtReminderTime);
            holder.txtvendername = (TextView) row.findViewById(R.id.txtvendername);
            holder.imgbtndownarrow = (ImageButton) row.findViewById(R.id.imgbtndownarrow);
            holder.imgbtnuparrow = (ImageButton) row.findViewById(R.id.imgbtnuparrow);
            holder.imgdelete = (ImageView) row.findViewById(R.id.imgdelete);
            holder.imgedit = (ImageView) row.findViewById(R.id.imgedit);
            holder.card_view = (CardView) row.findViewById(R.id.card_view);
            holder.taskmoredetaillayout = (LinearLayout) row.findViewById(R.id.moreDetailtask);
            holder.moredetailStartDate = (TextView) row.findViewById(R.id.reminderstartdate);
            holder.moredetailTaskRepeat = (TextView) row.findViewById(R.id.taskrepeat);
            holder.moredetailVenderMobile = (TextView) row.findViewById(R.id.vendermobileno);
            holder.imgcall = (ImageView) row.findViewById(R.id.imgcall);
            row.setTag(holder);
        } else {
            holder = (UserHolder) row.getTag();
        }

        final ReminderDetails reminderDetails = data.get(position);
        holder.txtReminderName.setText(reminderDetails.getReminderName());
        holder.moredetailVenderMobile.setText(context.getResources().getString(R.string.vendermobileno) + " " + reminderDetails.getServicemanNumber());
        holder.moredetailTaskRepeat.setText(context.getResources().getString(R.string.repeat) + " " + reminderDetails.getRemindertimePer());
        holder.moredetailStartDate.setText(context.getResources().getString(R.string.start_date) + " " + Getters.getdateStringddMMyyyy(context, reminderDetails.getReminderDate()));
        holder.txtasktime.setText(String.valueOf(Getters.getStringwhenInput24hrs(context, reminderDetails.getReminderTime().toString())));
        holder.txtvendername.setText("Vender Name " + " " + reminderDetails.getServicemanName());

        final UserHolder finalHolder = holder;
        holder.imgbtndownarrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finalHolder.taskmoredetaillayout.setVisibility(View.VISIBLE);
                finalHolder.imgbtndownarrow.setVisibility(View.GONE);
                finalHolder.imgbtnuparrow.setVisibility(View.VISIBLE);
            }
        });
        holder.imgbtnuparrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finalHolder.taskmoredetaillayout.setVisibility(View.GONE);
                finalHolder.imgbtnuparrow.setVisibility(View.GONE);
                finalHolder.imgbtndownarrow.setVisibility(View.VISIBLE);


            }
        });
        holder.imgcall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(reminderDetails.getServicemanNumber() != null) {
                    Toast.makeText(context, "call clicked", Toast.LENGTH_SHORT).show();
                    Intent callIntent = new Intent(Intent.ACTION_CALL);
                    callIntent.setData(Uri.parse("tel:" + reminderDetails.getServicemanNumber()));
                    if (ActivityCompat.checkSelfPermission(context, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                        // TODO: Consider calling
                        //    ActivityCompat#requestPermissions
                        // here to request the missing permissions, and then overriding
                        //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                        //                                          int[] grantResults)
                        // to handle the case where the user grants the permission. See the documentation
                        // for ActivityCompat#requestPermissions for more details.
                        ActivityCompat.requestPermissions((Activity) context,
                                new String[]{Manifest.permission.CALL_PHONE},   //request specific permission from user
                                10);
                        return;
                    } else {     //have got permission
                        try {
                            context.startActivity(callIntent);  //call activity and make phone call
                        } catch (android.content.ActivityNotFoundException ex) {
                            Toast.makeText(getContext(), "yourActivity is not founded", Toast.LENGTH_SHORT).show();
                        }
                    }
                }
               else
                {
                    Toast.makeText(context, "no no are specified", Toast.LENGTH_SHORT).show();
                }
            }
        });

        holder.card_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
             /*   SimpleDateFormat f = new SimpleDateFormat("MMM");
                 f.format(new Date());*/
                NotificationDataTable notificationDataTable=new NotificationDataTable();
                notificationDataTable.setReminderdateNotificationData(new Date());
                Intent intent = new Intent(context, CalenderView.class);
                intent.putExtra("reminderDetails",reminderDetails);
                getContext().startActivity(intent);
                update.onItemchange(true);
                //List<NotificationDataTable> reminderDetail = new  AddTaskActivityController().viewCalendar(context,reminderDetails,notificationDataTable);

            }
        });
        holder.imgdelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setTitle(context.getResources().getString(R.string.delete));
                builder.setCancelable(true);
                builder.setMessage((context.getResources().getString(R.string.taskdeletemessage)).concat(" "+reminderDetails.getReminderName()));
                builder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                      //  Toast.makeText(context, "Ok", Toast.LENGTH_SHORT).show();
                        new AddTaskActivityController().editparticularTask(reminderDetails,"delete",context);
                        update.onItemchange (true);

                    }
                })
                        .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        });
                builder.show();
                //Toast.makeText(context,"Delete Value Of Id = "+reminderDetails.getReminderId(), Toast.LENGTH_SHORT).show();
                //

            }
        });
        holder.imgedit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new AddTaskActivityController().editparticularTask(reminderDetails,"edit",context);
              // Toast.makeText(context,"Edit Value Of Id = "+reminderDetails.getReminderId(), Toast.LENGTH_SHORT).show();
                update.onItemchange (true);

            }
        });
        return row;

    }


    static class UserHolder {
        TextView txtReminderName;
        TextView txtasktime;
        TextView txtvendername;
        ImageButton imgbtndownarrow,imgbtnuparrow;
        ImageView imgdelete,imgedit,imgcall;
        CardView card_view;
        LinearLayout taskmoredetaillayout;
        TextView moredetailStartDate,moredetailVenderMobile,moredetailTaskRepeat;



    }
}