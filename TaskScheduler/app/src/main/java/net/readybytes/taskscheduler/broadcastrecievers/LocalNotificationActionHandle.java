package net.readybytes.taskscheduler.broadcastrecievers;

import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import net.readybytes.taskscheduler.Controller.DatabaseHandling;
import net.readybytes.taskscheduler.Controller.NotificationDataController;
import net.readybytes.taskscheduler.model.NotificationDataTable;
import net.readybytes.taskscheduler.model.ReminderDetails;

import java.util.Date;

public class LocalNotificationActionHandle extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        // TODO: This method is called when the BroadcastReceiver is receiving
        // an Intent broadcast.
        if(intent.getAction().equals("notification"))
        {
            new NotificationDataController().processNotification(context,intent);

        }

    }
}
