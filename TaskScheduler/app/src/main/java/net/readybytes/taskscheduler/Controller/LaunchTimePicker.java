package net.readybytes.taskscheduler.Controller;

import android.app.DatePickerDialog;
import android.app.PendingIntent;
import android.app.TimePickerDialog;
import android.content.Context;
import android.media.effect.Effect;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;

import net.readybytes.taskscheduler.globalusage.Getters;

import java.util.Calendar;

/**
 * Created by siddharh on 24/8/17.
 */

public class LaunchTimePicker {
    private Calendar calendar;
    String format;
    int selectedHour,selectedMinute;
    int yearofthisfunction,monthofthisfunction,dayofthisfunction;
    public void launchTimePicker(Context context, final EditText TimePickerTextView){
        calendar = Calendar.getInstance();
        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        int minute = calendar.get(Calendar.MINUTE);
        TimePickerDialog mTimePicker;
        mTimePicker = new TimePickerDialog(context, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int hourOfDay, int minute) {
                TimePickerTextView.setError(null);
                TimePickerTextView.setText(Getters.getStringwhenInputhoursandminutes(hourOfDay,minute,""));

            }
        }, hour, minute, false);//Yes 24 hour time
        mTimePicker.setTitle("Select Time");
        mTimePicker.show();
        //pass this selected hour and selected minute to getCalenderINstance method here
    }
    public void launchTimePickerforInputTextView(Context context, final TextView TimePickerTextView){
        calendar = Calendar.getInstance();
        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        int minute = calendar.get(Calendar.MINUTE);
        TimePickerDialog mTimePicker;
        mTimePicker = new TimePickerDialog(context, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int hourOfDay, int minute) {

                TimePickerTextView.setText(Getters.getStringwhenInputhoursandminutes(hourOfDay,minute,""));
            }
        }, hour, minute, false);//Yes 24 hour time
        mTimePicker.setTitle("Select Time");
        mTimePicker.show();
        //pass this selected hour and selected minute to getCalenderINstance method here
    }
    public void launchDatePicker(Context context, final EditText CalenderTextView, final Calendar calendarLocal){

        calendar = Calendar.getInstance();
        new DatePickerDialog(context, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                CalenderTextView.setError(null);
                CalenderTextView.setText(day+"/"+(++month)+"/"+year);
                calendarLocal.set(Calendar.YEAR,year);
                calendarLocal.set(Calendar.MONTH,(month-1));
                calendarLocal.set(Calendar.DAY_OF_MONTH,day);
            }
        },calendar.get(Calendar.YEAR),calendar.get(Calendar.MONTH),calendar.get(Calendar.DAY_OF_MONTH)).show();

    }
    public void launchDatePickerforInputTextView(Context context, final TextView CalenderTextView, final Calendar calendarLocal){

        calendar = Calendar.getInstance();
        new DatePickerDialog(context, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {

                CalenderTextView.setText(day+"/"+(++month)+"/"+year);
                calendarLocal.set(Calendar.YEAR,year);
                calendarLocal.set(Calendar.MONTH,(month-1));
                calendarLocal.set(Calendar.DAY_OF_MONTH,day);
            }
        },calendar.get(Calendar.YEAR),calendar.get(Calendar.MONTH),calendar.get(Calendar.DAY_OF_MONTH)).show();

    }
}

