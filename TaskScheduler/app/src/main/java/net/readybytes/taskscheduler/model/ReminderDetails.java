package net.readybytes.taskscheduler.model;

import java.io.Serializable;
import java.sql.Time;
import java.util.Date;

/**
 * Created by siddharh on 23/8/17.
 */

public class ReminderDetails implements Serializable{
    String reminderId;
    String reminderName;
    boolean isReminderActive;
    String servicemanName;
    String servicemanNumber;
    double costPerDay;
    long requestCode;
    byte[] servicemanPhoto;
    Time reminderTime;
    String remindertimePer;

    public String getWagesPerDayorMonth() {
        return wagesPerDayorMonth;
    }

    public void setWagesPerDayorMonth(String wagesPerDayorMonth) {
        this.wagesPerDayorMonth = wagesPerDayorMonth;
    }

    String wagesPerDayorMonth;
    public double getUnit() {
        return unit;
    }

    public void setUnit(double unit) {
        this.unit = unit;
    }

    public double getCostPerUnit() {
        return costPerUnit;
    }

    public void setCostPerUnit(double costPerUnit) {
        this.costPerUnit = costPerUnit;
    }

    Date reminderDate;
    double unit;
    double costPerUnit;
    public int getRepeatPerday() {
        return repeatPerday;
    }

    public void setRepeatPerday(int repeatPerday) {
        this.repeatPerday = repeatPerday;
    }

    public long getRequestCode() {
        return requestCode;
    }

    public void setRequestCode(long requestCode) {
        this.requestCode = requestCode;
    }

    int repeatPerday;
    public double getCostPerDay() {
        return costPerDay;
    }

    public void setCostPerDay(double costPerDay) {
        this.costPerDay = costPerDay;
    }

    public ReminderDetails() {
    }



    public ReminderDetails(String reminderName, boolean isReminderActive, String servicemanName, String servicemanNumber, byte[] servicemanPhoto,String remindertimePer,Time reminderTime, Date reminderDate,double costPerDay,int repeatperDay,long requestCode,double Unit,double CostPerUnit,String wagesPerDayorMonth) {
        this.reminderName       = reminderName;
        this.isReminderActive   = isReminderActive;
        this.servicemanName     = servicemanName;
        this.servicemanNumber   = servicemanNumber;
        this.servicemanPhoto    = servicemanPhoto;
        this.reminderTime       = reminderTime;
        this.remindertimePer    = remindertimePer;
        this.reminderDate       = reminderDate;
        this.costPerDay         = costPerDay;
        this.repeatPerday       = repeatperDay;
        this.requestCode        = requestCode;
        this.unit               = Unit;
        this.costPerUnit        = CostPerUnit;
        this.wagesPerDayorMonth = wagesPerDayorMonth;
    }

    public String getReminderId() {
        return reminderId;
    }

    public void setReminderId(String reminderId) {
        this.reminderId = reminderId;
    }

    public String getReminderName() {
        return reminderName;
    }

    public void setReminderName(String reminderName) {
        this.reminderName = reminderName;
    }

    public boolean isReminderActive() {
        return isReminderActive;
    }

    public void setReminderActive(boolean reminderActive) {
        isReminderActive = reminderActive;
    }

    public String getServicemanName() {
        return servicemanName;
    }

    public void setServicemanName(String servicemanName) {
        this.servicemanName = servicemanName;
    }

    public String getServicemanNumber() {
        return servicemanNumber;
    }

    public void setServicemanNumber(String servicemanNumber) {
        this.servicemanNumber = servicemanNumber;
    }

    public byte[] getServicemanPhoto() {
        return servicemanPhoto;
    }

    public void setServicemanPhoto(byte[] servicemanPhoto) {
        this.servicemanPhoto = servicemanPhoto;
    }

    public Time getReminderTime() {
        return reminderTime;
    }

    public void setReminderTime(Time reminderTime) {
        this.reminderTime = reminderTime;
    }

    public String getRemindertimePer() {
        return remindertimePer;
    }

    public void setRemindertimePer(String remindertimePer) {
        this.remindertimePer = remindertimePer;
    }

    public Date getReminderDate() {
        return reminderDate;
    }

    public void setReminderDate(Date reminderDate) {
        this.reminderDate = reminderDate;
    }
}
