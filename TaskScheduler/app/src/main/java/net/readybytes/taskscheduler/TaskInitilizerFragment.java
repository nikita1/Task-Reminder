package net.readybytes.taskscheduler;


import android.content.Intent;
import android.graphics.Point;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;

import net.readybytes.taskscheduler.Controller.DatabaseHandling;
import net.readybytes.taskscheduler.adapter.TaskListAdapter;
import net.readybytes.taskscheduler.model.ReminderDetails;
import net.readybytes.taskscheduler.view.AddTaskActivity;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class TaskInitilizerFragment extends Fragment implements View.OnClickListener,check_update_data {

    GridView grid;
    TaskListAdapter tasklistAdapter;
    ListView listtask ;
    LinearLayout layoutnoTask;
    static public int width;
    static public int height;
    ArrayList<ReminderDetails>  taskarraylist = new ArrayList<>();
    FloatingActionButton fab;
    Button btnAddTask;
    check_update_data updatenew  = this;
    SwipeRefreshLayout swipe_refresh_layout;
    DatabaseHandling databaseHandling;
    public TaskInitilizerFragment() {
        // Required empty public constructor
    }
    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        Display display = getActivity().getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        width = size.x;
        height = size.y;

        View v = inflater.inflate(R.layout.fragment_task_list, container, false);
        listtask = (ListView)v.findViewById(R.id.listTaskScheduled);
        swipe_refresh_layout= (SwipeRefreshLayout)v.findViewById(R.id.swipe_refresh_layout);
        fab= (FloatingActionButton)v.findViewById(R.id.floatbuttonaddtask);
        btnAddTask = (Button)v.findViewById(R.id.btnaddTask);
        layoutnoTask = (LinearLayout)v.findViewById(R.id.layoutNoTask);
        fatchAndInitilizeList();
        swipe_refresh_layout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                fatchAndInitilizeList();

            }
        });

        btnAddTask.setOnClickListener(this);
        fab.setOnClickListener(this);
        return v;


    }

    public void fatchAndInitilizeList() {
        databaseHandling= new DatabaseHandling(getContext());
        taskarraylist = (ArrayList<ReminderDetails>) databaseHandling.getListofallClients();
        if(taskarraylist != null && taskarraylist.size() != 0)
        {
            layoutnoTask.setVisibility(View.GONE);
            listtask.setVisibility(View.VISIBLE);
            tasklistAdapter = new TaskListAdapter(getContext(), R.layout.taskinfofill, taskarraylist, updatenew);
            listtask.setAdapter(tasklistAdapter);
            tasklistAdapter.setNotifyOnChange(true);
            swipe_refresh_layout.setRefreshing(false);

        }
        else if(taskarraylist!= null && taskarraylist.size() == 0)
        {
            layoutnoTask.setVisibility(View.VISIBLE);
            tasklistAdapter = new TaskListAdapter(getContext(), R.layout.taskinfofill, taskarraylist, updatenew);
            listtask.setAdapter(tasklistAdapter);
            tasklistAdapter.setNotifyOnChange(true);
            swipe_refresh_layout.setRefreshing(false);
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {


            if(requestCode == 200) {
            if(data!=null) {
                if (data.getBooleanExtra("result", false)) {
                    fatchAndInitilizeList();
                }
            }
            }
            if(requestCode == 100)
            {
                fatchAndInitilizeList();
            }


    }

    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.btnaddTask :
                Intent myIntent = new Intent(getContext(), AddTaskActivity.class);
                startActivityForResult(myIntent,200);
                getActivity().overridePendingTransition(R.anim.fragment_enter, R.anim.fragment_exit);
                break;
            case R.id.floatbuttonaddtask :
                Intent myIntentnew = new Intent(getContext(), AddTaskActivity.class);
                startActivityForResult(myIntentnew,200);
                getActivity().overridePendingTransition(R.anim.fragment_enter, R.anim.fragment_exit);
                break;

        }
    }

    @Override
    public void onItemchange(Boolean result) {
        if(result.equals(true))
        {
           fatchAndInitilizeList();

        }
    }


}
