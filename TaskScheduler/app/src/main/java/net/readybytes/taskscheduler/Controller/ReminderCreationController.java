package net.readybytes.taskscheduler.Controller;

import android.app.AlarmManager;
import android.content.Context;
import android.widget.Toast;

import net.readybytes.taskscheduler.R;
import net.readybytes.taskscheduler.model.ReminderDetails;

import java.sql.Time;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by siddharh on 23/8/17.
 */

public class ReminderCreationController {
   private ViewController viewController;
    AlarmManagerController alarmManagerController;
    Context context;
    public void addTasktoSqlliteAndLaunchAlarm(ReminderDetails reminderDetails, Context context,long requestcode,AlarmManager alarmManager,Time time,Date date,String action,Calendar calendar){
        viewController = new ViewController();
        //sending the complete reminderdetails object in sqllite database

        this.context = context;
        alarmManagerController = new AlarmManagerController();

        if(reminderDetails.getRemindertimePer().equalsIgnoreCase(context.getResources().getString(R.string.daily))){
            if(callTimePickertoSetAlarm((context.getResources().getString(R.string.daily)),reminderDetails,requestcode,alarmManager,time,date,action,calendar,context))
            {   //everything goes fine then launch BaseActivity by showing the message that the alarms has been set
                viewController.launchView("BaseActivity",context);
                
            }
        }else if(reminderDetails.getRemindertimePer().equalsIgnoreCase((context.getResources().getString(R.string.monthly)))){
            //code not written uptill yet
            if(callTimePickertoSetAlarm((context.getResources().getString(R.string.monthly)),reminderDetails ,requestcode,alarmManager,time,date,action,calendar, context)){
                viewController.launchView("BaseActivity",context);
            }
        }else if(reminderDetails.getRemindertimePer().equalsIgnoreCase((context.getResources().getString(R.string.no_repeating)))){
            if(callTimePickertoSetAlarm((context.getResources().getString(R.string.no_repeating)),reminderDetails ,requestcode,alarmManager,time,date,action,calendar, context)){
                viewController.launchView("BaseActivity",context);
            }
        }

    }
    //only for the day case,still many cases yet to be implemented
    public boolean callTimePickertoSetAlarm(String interval, ReminderDetails reminderDetails, long requestcode, AlarmManager alarmManager, Time time, Date date, String action, Calendar calendar, Context context){
        long intervallong=0;
       try {
                intervallong=calculateinterval(interval,calendar,context);
               if(alarmManagerController.startalarmManager(this.context,reminderDetails,requestcode, alarmManager, time,date,intervallong,action)){
                   Toast.makeText(this.context, "Alarm has been set", Toast.LENGTH_SHORT).show();
               }

           return true;
       }catch (Exception e){
           e.printStackTrace();
           return false;
       }
    }
    public long calculateinterval(String interval,Calendar calendar,Context context){
      long intervallong=0;
        if(interval.equals(context.getResources().getString(R.string.daily))){
            intervallong= AlarmManager.INTERVAL_DAY;
        }else if(interval.equals(context.getResources().getString(R.string.monthly))) {
            int monthMaxDays = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
            intervallong = AlarmManager.INTERVAL_DAY * monthMaxDays;
        }else if(interval.equals(context.getResources().getString(R.string.no_repeating))){
            intervallong = 0;
        }
    return intervallong;
    }


}
