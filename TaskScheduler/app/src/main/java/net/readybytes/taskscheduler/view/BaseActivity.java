package net.readybytes.taskscheduler.view;

import android.content.Intent;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import net.readybytes.taskscheduler.R;
import net.readybytes.taskscheduler.TaskInitilizerFragment;

public class BaseActivity extends AppCompatActivity {
    DrawerLayout drawer;
    FragmentManager mFragmentManager;
    FragmentTransaction mFragmentTransaction;
    NavigationView navigationView;
    android.support.v7.app.ActionBarDrawerToggle  mDrawerToggle;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_base);
        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        toolbar.setTitle("Task Schedular");
        setSupportActionBar(toolbar);
        drawer          = (DrawerLayout) findViewById(R.id.drawerLayout);
        navigationView  = (NavigationView) findViewById(R.id.nav_view);
        setSupportActionBar(toolbar);
        navigationView.setItemIconTintList(null);
        mDrawerToggle   = new ActionBarDrawerToggle(BaseActivity.this,drawer, toolbar,R.string.app_name, R.string.app_name);
        drawer.addDrawerListener(mDrawerToggle);
        mDrawerToggle.syncState();
        mFragmentManager    = getSupportFragmentManager();
        mFragmentTransaction= mFragmentManager.beginTransaction();
        mFragmentTransaction.replace(R.id.containerView, new TaskInitilizerFragment(),"taskAdd").commitAllowingStateLoss();
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener()
        {
            @Override

            public boolean onNavigationItemSelected(MenuItem menuItem) {
                drawer.closeDrawers();
                if(menuItem.getItemId() == R.id.action_share)
                {
                    Toast.makeText(BaseActivity.this, "share content", Toast.LENGTH_SHORT).show();
                    return true;
                }
                if(menuItem.getItemId()== R.id.scheduldedAlarms){
                   mDrawerToggle.setDrawerIndicatorEnabled(true);
                    setSupportActionBar(toolbar);
                    getSupportActionBar().setHomeButtonEnabled(true);
                    getSupportActionBar().setDisplayShowHomeEnabled(true);
                    mDrawerToggle.syncState();
                    toolbar.setTitle("Schedulded Task");
                    mFragmentManager    = getSupportFragmentManager();
                    mFragmentTransaction= mFragmentManager.beginTransaction();
                    mFragmentTransaction.replace(R.id.containerView, new Task_Enable_Disable(),"scheduleAlarms").commitAllowingStateLoss();
                    return true;
                }
                if(menuItem.getItemId() == R.id.taskadd)
                {
                    mDrawerToggle.setDrawerIndicatorEnabled(true);
                    setSupportActionBar(toolbar);
                    getSupportActionBar().setHomeButtonEnabled(true);
                    getSupportActionBar().setDisplayShowHomeEnabled(true);
                    mDrawerToggle.syncState();
                    toolbar.setTitle("Add Task");
                    mFragmentManager    = getSupportFragmentManager();
                    mFragmentTransaction= mFragmentManager.beginTransaction();
                    mFragmentTransaction.replace(R.id.containerView, new TaskInitilizerFragment(),"taskAdd").commitAllowingStateLoss();
                    return true;
                }


                return false;
            }
        });


    }


    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.share, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_share)
        {

            return true;
        }
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
  @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // find your fragment as you need, here is by id

        // Pass the activity result to the fragment
       if(requestCode == 100)
       {
           Fragment fragment = mFragmentManager.findFragmentByTag("taskAdd");
           fragment.onActivityResult(requestCode, resultCode, data);
       }

    }
}
