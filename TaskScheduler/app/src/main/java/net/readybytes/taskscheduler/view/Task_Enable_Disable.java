package net.readybytes.taskscheduler.view;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import net.readybytes.taskscheduler.Controller.DatabaseHandling;
import net.readybytes.taskscheduler.R;
import net.readybytes.taskscheduler.adapter.NotificationTasklistAdapter;
import net.readybytes.taskscheduler.check_update_data;
import net.readybytes.taskscheduler.model.ReminderDetails;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class Task_Enable_Disable extends Fragment implements check_update_data {
    NotificationTasklistAdapter tasklistAdapter;
ListView listtask ;
    check_update_data updatenew  = this;
    DatabaseHandling databaseHandling;
    ArrayList<ReminderDetails>  taskarraylist = new ArrayList<>();
    public Task_Enable_Disable() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_task__enable_disable_list, container, false);
        databaseHandling= new DatabaseHandling(getContext());
        listtask = (ListView)v.findViewById(R.id.lvtasklist);
        initilizeNotificationEnableDisableList();

        return v;

    }

    private void initilizeNotificationEnableDisableList() {
        taskarraylist = (ArrayList<ReminderDetails>) databaseHandling.getListofallClients();
        tasklistAdapter = new NotificationTasklistAdapter(getContext(), R.layout.listofalarmsdesign, taskarraylist,updatenew);
        listtask.setAdapter(tasklistAdapter);
        tasklistAdapter.setNotifyOnChange(true);


    }
    @Override
    public void onItemchange(Boolean result) {
        if(result.equals(true))
        {
            initilizeNotificationEnableDisableList();

        }
    }

}
