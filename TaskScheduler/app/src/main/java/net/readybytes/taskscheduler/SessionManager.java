package net.readybytes.taskscheduler;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import com.google.gson.Gson;

import java.util.Date;
import java.util.HashMap;

/**
 * Created by root on 5/9/16.
 */
public class SessionManager {
    SharedPreferences pref;
    Editor editor;
    Context _context;
    int PRIVATE_MODE = 0;
    Gson gson;

     private static final String PREF_NAME = "TaskSchedularSession";

    public SessionManager(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }


    /**Remember this has been called for the first time, when AlarmReciever has been called,So take Caution while calling getLastDate    **/
    public void setLastDate(String Key_TASKNAME, Date lastDate) {
        gson =  new Gson();
         String date  =  gson.toJson(lastDate);
        editor.putString(Key_TASKNAME, date);
        editor.commit();

    }

    // get splash status  user are seen the splash or not
    public Date getLastDate(String Key_TASKNAME) {
        gson=new Gson();
        Date date=gson.fromJson(pref.getString(Key_TASKNAME, null), Date.class);
        return date;
    }

}


