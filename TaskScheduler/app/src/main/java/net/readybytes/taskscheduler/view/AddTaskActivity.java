package net.readybytes.taskscheduler.view;

import android.app.AlarmManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Point;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import net.readybytes.taskscheduler.Controller.DatabaseHandling;
import net.readybytes.taskscheduler.Controller.LaunchTimePicker;
import net.readybytes.taskscheduler.Controller.ReminderCreationController;
import net.readybytes.taskscheduler.R;
import net.readybytes.taskscheduler.globalusage.Getters;
import net.readybytes.taskscheduler.model.ReminderDetails;

import java.io.ByteArrayOutputStream;
import java.sql.Time;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;

public class AddTaskActivity extends AppCompatActivity implements View.OnClickListener {
    EditText taskNameEdittext;
    Point p;
    static boolean validationPerfectAndProceedFurther=true;
    PopupWindow popupWindow;
    Spinner  RepeatSpinnerOption,WagesPerDayOrMonth;
    EditText ReminderTimeSetter;
    TextView txtrepeatNoofTime;
    EditText ReminderDateSetter;
    EditText VendorNameEditText;
    EditText VendorNumberEditText;
    Button doneButton,nextButton;
    Time time;
    EditText UnitEdittext,CostPerUnitEditText;
    Context context;
   // static int repeatePopupNoOfTime = 0;
    Date date;
    ImageView clockimageview,calenderImageiew;
    AlarmManager alarmManager;
    Calendar calendar= Calendar.getInstance();
    int width;
    int height;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_task);
      Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

        taskNameEdittext = (EditText)findViewById(R.id.taskNameeditText);

        RepeatSpinnerOption = (Spinner) findViewById(R.id.repeatOptionSpinner);
   //     doneButton = (Button)findViewById(R.id.donebutton);
     nextButton = (Button)findViewById(R.id.nextButton);
         setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.backblue);
        ReminderTimeSetter = (EditText)findViewById(R.id.reminderTimeEditText);
        ReminderDateSetter = (EditText)findViewById(R.id.reminderDateEditText);
     //   VendorNameEditText = (EditText)findViewById(R.id.venderNameEdittext);
       // VendorNumberEditText = (EditText)findViewById(R.id.venderMobileNumberEdittext);
        //UnitEdittext=(EditText)findViewById(R.id.QuantityEdittext);
        CostPerUnitEditText=(EditText)findViewById(R.id.venderPerDayPriceEdittext);
        alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
      //  WagesPerDayOrMonth=(Spinner)findViewById(R.id.wagesPerDayOrMonth) ;
        clockimageview=(ImageView)findViewById(R.id.reminderTimeImageView);
        calenderImageiew=(ImageView)findViewById(R.id.reminderDateImageView);

        ReminderDateSetter.setOnClickListener(this);
        nextButton.setOnClickListener(this);
        context = this;
        ReminderDateSetter.setText(Getters.getdateStringddMMyyyy(context,new Date()));
        int hour=new Date().getHours();
        int minutes=new Date().getMinutes();
        ReminderTimeSetter.setText(Getters.getStringwhenInput24hrs(context,hour+":"+minutes+":00").toString());
        ReminderTimeSetter.setOnClickListener(this);
//        doneButton.setOnClickListener(this);
        clockimageview.setOnClickListener(this);
        calenderImageiew.setOnClickListener(this);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                overridePendingTransition(R.anim.animation_enter, R.anim.animation_exit);
            }
        });

        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(this, R.layout.spinner_item,
                getResources().getStringArray(R.array.repeatItem))
        {
            @Override
            public boolean isEnabled(int position){
                if(position == 0)
                {
                    // Disable the first item from Spinner
                    // First item will be use for hint
                    return false;
                }
                else
                {
                    return true;
                }
            }
            @Override
            public View getDropDownView(int position, View convertView,
                                        ViewGroup parent)
            {
                View view = super.getDropDownView(position, convertView, parent);
                TextView tv = (TextView) view;
                if(position == 0){
                    // Set the hint text color gray
                    tv.setTextColor(Color.GRAY);
                }
                else {
                    tv.setTextColor(Color.BLACK);
                }
                return view;
            }
            };

        spinnerArrayAdapter.setDropDownViewResource(R.layout.spinner_item);
        RepeatSpinnerOption.setAdapter(spinnerArrayAdapter);




        }


/*
    public void addTaskintoReminderDetailsObject(){
        long requestCode=(long)System.currentTimeMillis();
        Bitmap largeIcon = BitmapFactory.decodeResource(getResources(), R.drawable.thumbsup);
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        largeIcon.compress(Bitmap.CompressFormat.JPEG, 100, stream);
        byte[] byteArray = stream.toByteArray();

             ReminderDetails reminderDetails = new ReminderDetails(taskNameEdittext.getText().toString(),
             true,
             VendorNameEditText.getText().toString(),
             VendorNumberEditText.getText().toString(),
             byteArray,
             RepeatSpinnerOption.getSelectedItem().toString(),
             time,
             date,
             CostPerUnitEditText.getText().toString(),
             1,
             requestCode,
             0,
             0.0,
             WagesPerDayOrMonth.getSelectedItem().toString());
        new ReminderCreationController().addTasktoSqlliteAndLaunchAlarm(reminderDetails,AddTaskActivity.this,requestCode,alarmManager,time,date,"create",calendar);
    }*/

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.reminderTimeImageView:
                new LaunchTimePicker().launchTimePicker(AddTaskActivity.this, ReminderTimeSetter);
                break;
            case R.id.reminderDateImageView:
                new LaunchTimePicker().launchDatePicker(AddTaskActivity.this, ReminderDateSetter, calendar);
                break;
            case R.id.reminderTimeEditText:
                new LaunchTimePicker().launchTimePicker(AddTaskActivity.this, ReminderTimeSetter);
                break;
            case R.id.reminderDateEditText:
                new LaunchTimePicker().launchDatePicker(AddTaskActivity.this, ReminderDateSetter, calendar);
                break;
           /* case R.id.donebutton:
                Boolean validationchek = checkvalidation();
                if(validationchek)
                {
                    *//*addTaskintoReminderDetailsObject();
                    Intent intent = getIntent();
                    Bundle bundle = new Bundle();
                    bundle.putBoolean("isModify", true);
                    intent.putExtras(bundle);
                    setResult(999, intent);
                    finish();*//*
                }
                else
                {
                    return;
                }

                break;*/
            case R.id.nextButton:
                Boolean validationcheck = checkvalidation();
                if(validationcheck) {

                    String taskName = taskNameEdittext.getText().toString();
                    Intent intent = new Intent(context, AddTaskActivityFinalScreen.class);
                    intent.putExtra("TaskName", taskName);
                    intent.putExtra("RepeatationType", RepeatSpinnerOption.getSelectedItem().toString());
                    intent.putExtra("time", time);
                    intent.putExtra("date", date);
                    startActivityForResult(intent,200);
                }
                else {
                    return;
                }
        }
    }

    private Boolean checkvalidation() {
        String taskname = taskNameEdittext.getText().toString();
        Boolean taskalreadycreated = false;
        if (taskname.equals(""))
        {
            taskNameEdittext.setError(this.getResources().getString(R.string.validation_TaskNameEmpty));
            return false;
        }
      else
        {
            LinkedList<String> list = new DatabaseHandling(context).getListofTasksName();
            for (String node:
                    list)
            {
                if(taskname.equals(node)){
                    taskalreadycreated = true;
                    break;
                }

            }
            if(taskalreadycreated)
            {
             taskNameEdittext.setError(this.getResources().getString(R.string.validation_sametask_error));

                return false;
            }

        }
if(RepeatSpinnerOption.getSelectedItem().equals(getResources().getString(R.string.selectrepeatinterval)))
{

    View selectedView = RepeatSpinnerOption.getSelectedView();
    if (selectedView != null && selectedView instanceof TextView)
    {
        TextView errorText = (TextView)RepeatSpinnerOption.getSelectedView();
        errorText.setError("");
        errorText.setTextColor(Color.RED);//just to highlight that this is an error
        errorText.setText(getResources().getString(R.string.error_repeatinterval));
    }
    return false;
}
        int hours = new Date().getHours();
        int minutes = new Date().getMinutes();
        Time timeforcomparison = Time.valueOf(hours + ":" + minutes + ":00");

        date = Getters.getdateasDateObject(getBaseContext(), ReminderDateSetter.getText().toString());
        time = Getters.getTimeObjectwhenInputAMPM(getBaseContext(), ReminderTimeSetter.getText().toString());

        if (Getters.getDateinyyyyMMddHHmmformatAsDateObject(context, time, date).compareTo(Getters.getDateinyyyyMMddHHmmformatAsDateObject(context, timeforcomparison, new Date())) >= 0)
        {

        }//only if you don't want to have time before the current time
        /*else {
            Toast.makeText(context, R.string.validation_TimeandDateAfterCurrentTime, Toast.LENGTH_SHORT).show();
            return false;
        }*/
        return true;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if(id == android.R.id.home){

            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.addtaskmenu, menu);
        return true;
    }

    @Override
    public void onBackPressed() {

        super.onBackPressed();
        overridePendingTransition(R.anim.animation_enter, R.anim.animation_exit);
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == 200) {
            Intent intent = data;
            if(data.getBooleanExtra("result",false))
            {
                setResult(200, intent);
                this.finish();
            }

        }
        }


}
